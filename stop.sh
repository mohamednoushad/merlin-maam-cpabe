cd $PWD/platfrom-client
pm2 stop client
pm2 delete client

cd ..

cd $PWD/platform-server
pm2 stop index.js
pm2 delete index.js

cd ..


cd $PWD/fhir-server
pm2 stop fhir
pm2 delete fhir


if [ $(docker ps -a -q) ]
then
    docker stop $(docker ps -a -q)
    docker rm $(docker ps -a -q)
fi


