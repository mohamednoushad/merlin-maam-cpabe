var ethers = require('ethers');
var config = require('./config');

//var provider = ethers.providers.getDefaultProvider('rinkeby');

var provider = new ethers.providers.JsonRpcProvider('https://rinkeby.infura.io/v3/62349edb370e4523b328b8823d211551', 'rinkeby');

//http://rinkeby.infura.io/v3/62349edb370e4523b328b8823d211551

function instantiateWallet(privateKey) {
    var wallet = new ethers.Wallet(privateKey);
    wallet.provider = new ethers.providers.JsonRpcProvider('https://rinkeby.infura.io/v3/62349edb370e4523b328b8823d211551', 'rinkeby');
    return wallet;
}


module.exports = {
    provider: new ethers.providers.JsonRpcProvider('https://rinkeby.infura.io/v3/62349edb370e4523b328b8823d211551', 'rinkeby'),

    transferEthers: async (masterPrivateKey, userAddress) => {
        try {
            const amount = ethers.utils.parseEther('0.5');
            const wallet = instantiateWallet(masterPrivateKey);
            const tx = await wallet.send(userAddress, amount);
            if(tx && tx.hash) {
                await provider.waitForTransaction(tx.hash);
                const status = await provider.getTransactionReceipt(tx.hash);
                if (status.status === 1) {
                    return {
                        status: 200,
                        message: "Transaction mined. Execution success"
                    };
                }
                else if (status.status === 0){
                    throw {
                        status: 500,
                        message: "Transaction mined but execution failed."
                    }
                }
            }
            else {
                throw {
                    status: 500,
                    message: "Transaction failed.",
                    error: tx
                }
            }
        }
        catch (e) {
            throw {
                status : 500,
                error:e
            }
        }
    },

    hasAnyOneGivenAccessToDoctor: async (doctorAddress) => {

        console.log("calling has anyone given access api");

        const contract = new ethers.Contract(config.contractAddress,config.abi,provider);

        const patientAddress = await contract.accessGivenPatientAddress(doctorAddress);

        if (patientAddress=="0x0000000000000000000000000000000000000000"){
            return {status:false,patientAddress:null}
        }else{
            return {status:true,patientAddress:patientAddress};
        }


    },

    giveAccess : async (pvtKey, docAddress, downloadUrl,expiryTime) => {

        var wallet = new ethers.Wallet(pvtKey,provider);
        const contract = new ethers.Contract(config.contractAddress,config.abi,wallet);
         console.log("here");

         try {

            const tx = await contract.giveAccess(downloadUrl,docAddress,+(expiryTime));

            console.log(tx);
            console.log("waiting for blockhain transaction");
            
            await provider.waitForTransaction(tx.hash);
    
            console.log("transaction successful");
            console.log(tx);
             
         } catch (error) {

            console.log(error);
             
         }
       

    },

    revokeAccess : async (pvtKey, patAddress) => {

        var wallet = new ethers.Wallet(pvtKey,provider);
        const contract = new ethers.Contract(config.contractAddress,config.abi,wallet);

        const tx = await contract.revokeAccess(patAddress)

        console.log(tx);
        console.log("waiting for blockhain transaction");
        
        await provider.waitForTransaction(tx.hash);

        console.log("transaction successful");
        console.log(tx);


    },

    getFileUrl : async (doctorAddress) => {

        const contract = new ethers.Contract(config.contractAddress,config.abi,provider);

        const fileUrl = await contract.getUrl(doctorAddress);

       
        console.log("this is file url from smart contract",fileUrl);

        return fileUrl;      


    }

}