import transaction from '../transaction';

module.exports = {

    transferEthers: async (masterPrivateKey, account) => {

        console.log("transfering ether function to account",account);
        
        try {
            return await transaction.transferEthers(masterPrivateKey, account);
        }
        catch (e) { throw e; }
    }


}