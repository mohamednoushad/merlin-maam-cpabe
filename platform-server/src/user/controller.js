import transaction from "../transaction";
import config from "../config";
const request = require("request");


module.exports = {

    checkIfPatientGivenAccess : async function (doctorAddress) {

        return await transaction.hasAnyOneGivenAccessToDoctor(doctorAddress);

    },

    giveAccessTransaction: async function (pvtKey, doctorAddress, downloadUrl,expiryTime) {

        console.log("calling give access transaction");

        return await transaction.giveAccess(pvtKey,doctorAddress,downloadUrl,expiryTime);


    },

    getFileUrlOfPatient : async function (doctorAddress) {

        return await transaction.getFileUrl(doctorAddress);

    },


    revokeAccess : async function (pvtKey, patientAddress) {

        return await transaction.revokeAccess(pvtKey,patientAddress);



    }

}