
import express from "express";
const router = express.Router();
const db = require("./userdb");
import keythereum from '../keystore/keythereum';
const config = require('../config');
const fhir = require("../fhir");
import sha1 from 'sha1';
import transaction from "./transact";
import controller from "./controller";

router.get('/clearDb', async (req, res, next) => {

    try {

        await db.clearData();

        res.status(200).send("done");

    }
    catch (e) { res.status(500).send(e); }
})

router.post('/register', async (req, res, next) => {

    try {
        const data = req.body;

        if(data.userType && data.userType == "doctor" || data.userType == "patient") {

            const userExistence = await db.checkIfUserExist(data.username);

            if(userExistence){
                res.send({message:"user already exist"});
            } else {
                console.log("creating keythereum file");
                const keystore = await keythereum.generateKey(data.password);
                console.log("saving keystore file");
                await keythereum.exportToFile(keystore, config.datadir);
                console.log("saving user to db");
                await db.insertUser(data.username,data.userType,"0x"+keystore.address);
                const userIdGenerated = await db.getUserId(data.username);
                console.log("adding ethers to account");
                await transaction.transferEthers(config.admin_pk, "0x" + keystore.address);
                
                if(data.userType == "patient") {
                    data.emrId = sha1(data.username + Date.now());
                    console.log("inserting identifier",data.emrId);
                    await db.updateIdentifierForPatient(data.username,data.emrId);
                    console.log("this is data",data);
                    await fhir.addPatient(data);
                } 
                res.status(200).send({message:"success",userId:userIdGenerated}); 
             }

        }else{
            res.send({message:"userType is not present"});
        }
    }
    catch (e) { res.status(500).send(e); }
})

router.post('/login', async (req, res, next) => {

    try {

        const data = req.body;


        if(data.userType && data.userType == "doctor" || data.userType == "patient") {

            const userExistence = await db.checkIfUserExist(data.username);

            if(userExistence){

                const user  =await db.getUserDetails(data.username);

                const privateKey = await keythereum.getPrivateKey(user.address,data.password);

                if(privateKey) {

                    if(data.userType == "doctor") {

                        res.status(200).send({message:"success",userType:"doctor",userDetails:user});

                    } else {

                        res.status(200).send({message:"success",userType:"patient",userDetails:user});

                    }
       

                } else {

                    res.status(200).send({message:"Incorrect Login Details"});

                }

            } else {

                res.send({message:"user doesnot exist"});
         

            }

        }else{

            res.send({message:"userType is not present"});
        }
    }
    catch (e) { res.status(500).send(e); }
})

router.get('/doctor/hasAnyOneGivenAccess', async (req, res, next) => {

    const data = req.query;

    let doctorAddress = data.address;

    try {

        const hasPatientGivenAccess = await controller.checkIfPatientGivenAccess(doctorAddress);

        if(hasPatientGivenAccess.status){
            const patientName = await db.getNameOfUserFromAddress(hasPatientGivenAccess.patientAddress);
            res.status(200).send({message:"success",patientAddress:hasPatientGivenAccess.patientAddress, patientName:patientName})
        } else {
            res.status(200).send({message:"noaccessgiven"})
        }
    }
    catch (e) { res.status(500).send(e); }
})

router.get('/patient/profile', async (req, res, next) => {

    const data = req.query;

    let emrIdentifier = data.identifier;

    try {

        const patientProfileData = await fhir.getUserProfile(emrIdentifier);
        res.status(200).send({patientProfile:JSON.parse(patientProfileData)});

    }
    catch (e) { res.status(500).send(e); }
})

router.get('/patient/medication', async (req, res, next) => {

    const data = req.query;

    let patientId = data.id;

    try {

        const patientMedicines = await fhir.getPrescriptions(patientId);
        res.status(200).send({medications:JSON.parse(patientMedicines)});

    }
    catch (e) { res.status(500).send(e); }
})

router.post('/patient/getDocotorIdFromAddress', async (req, res, next) => {

    const data = req.body;

    let doctorAddress = data.address;

    try {

        const idOfDoctor = await db.getIdFromAddress(doctorAddress);

       res.send({id:idOfDoctor});

    }
    catch (e) { res.status(500).send(e); }
})



router.get('/patient/giveAccess', async (req, res, next) => {

    const data = req.query;

    let doctorAddress = data.doctorAddress;
    let url = data.downloadUrl;
    let token = data.token;
    let patientUserName = data.patient;
    let expiryTime = data.expirytime;
 
    let totalUrl = url+'&token='+token;

    console.log(totalUrl);

     try {

        const user  = await db.getUserDetails(patientUserName);

        const privateKey = await keythereum.getPrivateKey(user.address,'password');

        await controller.giveAccessTransaction(privateKey,doctorAddress,totalUrl,expiryTime)

        res.send({message:"success"});

    }
    catch (e) { res.status(500).send(e); }
})


router.get('/doctor/getFileUrl', async (req, res, next) => {

    const data = req.query;

    let doctorAddress = data.doctorAddress;

    console.log("this is the doctor address",doctorAddress);

     try {

        const fileUrl = await controller.getFileUrlOfPatient(doctorAddress);

        console.log("this is fileUrl",fileUrl);

        res.send({message:"success",url:fileUrl});

    }
    catch (e) { res.status(500).send(e); }
})


router.get('/patient/revokeAccess', async (req, res, next) => {

    const data = req.query;

    let patientAddress = data.patientAddr;

    // let patientUserName = data.patient;


     try {

        // const user  = await db.getUserDetails(patientUserName);

        const privateKey = await keythereum.getPrivateKey(patientAddress,'password');

        await controller.revokeAccess(privateKey,patientAddress);

        res.send({message:"success"});

    }
    catch (e) { res.status(500).send(e); }
})






module.exports = router;