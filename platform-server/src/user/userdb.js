import DBService from "../services/dbservice";

async function checkIfUserExist(username) {
    try{
    
        let userQuery = `SELECT * FROM users WHERE username = '${username}'`;
        let userResult = await DBService.executeQuery({query: userQuery, values: []});

        console.log(userResult);

        if(userResult.length > 0) {
            return true;
        }else {
            return false;
        }

    }catch(err){
        throw err;
    }
}

async function getUserDetails(username) {
    try{
    
        let userQuery = `SELECT * FROM users WHERE username = '${username}'`;
        let userResult = await DBService.executeQuery({query: userQuery, values: []});

        console.log(userResult);

        if(userResult.length > 0) {
            return userResult[0];
        }else {
            return false;
        }

    }catch(err){
        throw err;
    }
}

async function insertUser(username,userType,address) {
    try{
        await DBService.insertQuery({table:'users',columns:['username','usertype','address'], values:[[username,userType,address]]})
    }catch(err){
        throw err;
    }
}


async function getUserId(username) {

    try{
    
        let userQuery = `SELECT id FROM users WHERE username = '${username}'`;
        let userResult = await DBService.executeQuery({query: userQuery, values: []});

         if(userResult.length > 0) {
            return userResult[0].id;
        }else {
            return false;
        }

    }catch(err){
        throw err;
    }
}


async function updateIdentifierForPatient (username,emrId) {

    try{
    
        let query = `UPDATE users SET identifier = '${emrId}' WHERE username = '${username}'`;
        await DBService.executeQuery({query: query, values: []});

    }catch(err){
        throw err;
    }


}

async function getNameOfUserFromAddress (useraddress) {

    try{
    
        let query = `SELECT username FROM users WHERE address = '${useraddress}'`;
        const result = await DBService.executeQuery({query: query, values: []});

        if(result.length > 0) {
            return result[0].username;
        }else {
            return false;
        }

    }catch(err){
        throw err;
    }


}

async function getIdFromAddress(address) {

    let query = `SELECT id FROM users WHERE address = '${address}'`;
    const result = await DBService.executeQuery({query: query, values: []});

    if(result.length > 0) {
        return result[0].id;
    }else {
        return false;
    }
}

async function clearData() {

    try{
    
        let query = `TRUNCATE TABLE users`;
        await DBService.executeQuery({query: query, values: []});

    }catch(err){
        throw err;
    }

    
}



module.exports = {
    checkIfUserExist,
    insertUser,
    getUserDetails,
    getUserId,
    updateIdentifierForPatient,
    clearData,
    getNameOfUserFromAddress,
    getIdFromAddress
};