let config = require("./config");
const request = require("request");

function requestOpenMRS(payload) {
    let req;
    const { method, url, query, body, department, hospital } = payload;
    switch (method) {
        case "GET":
            req = { method, url, qs: query }
            break;
        case "POST":
            req = { method, url, body, json: true }
            break;
        case "PATCH":
            req = { method, url, body, json: true }
            break;
        case "PUT":
            req = { method, url, body, json: true }
            break;
    }
    req.headers = {
        'Cache-Control': 'no-cache',
        Authorization: 'Basic YWRtaW46QWRtaW4xMjM=',
        'Content-Type': 'application/json+fhir'
    };
    console.log(req);
    return new Promise((resolve, reject) => {
        request(req, function (error, response, body) {
            if (error || (body && body.issue && body.issue.length && body.issue[0].diagnostics)) {
                let err = error || { message: body.issue[0].diagnostics };
                reject(err);
            }
            else {
                console.log("Data obtained  from emr", body);
                if(payload.encapsulate){
                    resolve({body, department, hospital});
                }
                else {
                    resolve(body);
                }
            }
        });
    });
}


module.exports = {

    addPatient : async (data) => {

        let body = {
            "resourceType": "Patient",
            id: data.emrId,
            "identifier": [{
                "use": "usual",
                "system": "Old Identification Number",
                "value": data.emrId
            }],
            "name": [{
                "use": "usual",
                "family": [data.familyName],
                "given": [data.firstName],
                "prefix": ["Mr."],
                "suffix": ["Mr."]
            }],
            "gender": data.gender,
            "birthDate": data.dob.split('T')[0],
            "deceasedBoolean": false,
            "address": [{
                "use": "home",
                "type": "postal",
                "line": [data.addressLine1, data.addressLine2],
                "text": data.addressLine1 + ', ' + data.addressLine2,
                "city": data.city,
                "state": data.state,
                "postalCode": data.areaCode,
                "country": data.country
            }],
            "active": true
        };

        const payload = {
            method: 'POST',
            url: config.fhirServerUrl + '/Patient',
            body,
        }
        return requestOpenMRS(payload);
    },


    getUserProfile : async (emrIdentifier) => {

     
        const payload = {
            method: 'GET',
            url: config.fhirServerUrl + '/Patient',
            query: {
                identifier: emrIdentifier,
            }
        };

        return requestOpenMRS(payload);

    },

    getPrescriptions: function (personId) {
        return new Promise(async (resolve, reject) => {
  
            const payload = {
                method: 'GET',
                url: config.fhirServerUrl + '/MedicationRequest',
                query: {
                    patient: `Patient/${personId}`,
                }
            };
            try {
                let data = await requestOpenMRS(payload);
                resolve(data);
            }
            catch (err) {
                console.log(err);
                reject(err);
            }
        });
    }

}


