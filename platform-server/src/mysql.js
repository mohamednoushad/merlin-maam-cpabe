const mysql = require('mysql');
const  config = require('./config');

const connection = mysql.createConnection({
    host: config.db.host,
    user: config.db.user,
    password: config.db.password,
    database: config.db.database
});

module.exports = {
    createDBConnection: function () {
        return new Promise ((resolve, reject) => {
            connection.connect(function (err) {
                if (err) {
                    reject(err);
                }
                else {
                    resolve();
                }
            });
        });
    },

    query: function (query, params) {
        return new Promise ((resolve, reject) => {
            connection.query(query, params, function (err, results, fields)  {
                if (err) {
                    console.log(err);
                    reject(err);
                }
                else {
                    console.log(results);
                    resolve(results);
                }
            });
        });
    },

    escape: (data) => {
        return connection.escape(data);
    }
}

