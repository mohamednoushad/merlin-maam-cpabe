const config = require("../config");

const ethers = require('ethers');
const provider = ethers.providers.getDefaultProvider('rinkeby');
const Interface = ethers.Interface;
const iface = new Interface(config.abi);
const eventInfo = iface.events;

module.exports = {

    getAllEventLogs: async (eventTopic,eventName,address) => {

        var filter = {
            fromBlock: 5794523,
            address: config.contractAddress,
            topics: [ eventTopic ]
         }

         var eventLogs = await provider.getLogs(filter);

         let a = [];

         for (const i in eventLogs) {

            let b = {};

            let newEventInfo = eventInfo[eventName];
 
            var eventParsed = newEventInfo.parse(eventLogs[i].topics, eventLogs[i].data);
 
            if(eventParsed._patient.toLowerCase() == address) {

                b.eventType  = eventName;
                b.doctor = eventParsed._doctor;
                let date = new Date(eventParsed.time.toNumber()*1000).toLocaleDateString();
                let time = new Date(eventParsed.time.toNumber()*1000).toLocaleTimeString();
                b.date = date + " " + time;

                a.push(b);

            }

         }

         console.log(a);

         return a;

    }

}