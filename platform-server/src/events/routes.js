import express from "express";
const router = express.Router();
const controller = require('./controller');

router.get('/accessGiven', async (req, res, next) => {

    console.log(req.query);

    let data = req.query;

    let patientAddr = data.patientAddress;


    try {

        let eventTopic = "0xfbd23e7b150d9b61337afb00d7ad79f8a400ee3d8471c124a8083e1e237a1811";

        let eventName = "accessGiven"

        const events = await controller.getAllEventLogs(eventTopic,eventName,patientAddr);

        res.send({events:events});


    }
    catch (e) { res.status(500).send(e); }
})

router.get('/accessRevoked', async (req, res, next) => {

    console.log("calling access revoked events api");

    console.log(req.query);

    let data = req.query;

    let patientAddr = data.patientAddress;


    try {

        let eventTopic = "0xe003efb8703e203c4a8e1cf7bc9cdd704f0e3877660d627331c604f49c673c37";

        let eventName = "accessRevoked"

        const events = await controller.getAllEventLogs(eventTopic,eventName,patientAddr);

        res.send({events:events});


    }
    catch (e) { res.status(500).send(e); }
})


module.exports = router;