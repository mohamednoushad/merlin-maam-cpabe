import express from 'express';
import PATH from 'path';
const router = express.Router();

const userRouter = require(PATH.resolve('src/user'));
const eventsRouter = require(PATH.resolve('src/events'));

router.use('/user', userRouter);
router.use('/events', eventsRouter);



module.exports = router;