var express = require('express');
var cors = require('cors');
var bodyParser = require('body-parser');
var app = express();
const fileUpload = require('express-fileupload');
var mkdirp = require('mkdirp');
const routes = require('./routes');
var os = require('os');
const config = require('./config');

const mysql = require('./mysql');


app.use(cors());
app.use(bodyParser.json());


app.use('/api', routes);

app.use(fileUpload());

// var datadir = process.cwd()+'/keystore';


const port = 5000;


function initializeServer() {
    mkdirp(config.datadir, function (err) {
      if (err) {
        console.error(err);
        res.status(500).send(err);
      }
      else {
        console.log('Directory Created');
        mysql.createDBConnection()
          .then(res => {
            console.log("Connected!");
            app.listen(port, function () {
              console.log(`platform server listening on ${port}`);
            });
          })
          .catch(e => log.error(e));
      }
    });
  
  }


initializeServer();