exports.db = {
    host: "localhost",
    user: "root",
    password: "attinad@123",
    database: "cpabe"
};

exports.datadir = process.cwd()+'/keystore';

exports.admin_pk = "0xF6F73ED9A00412658BFD82BCBEE2435764D97AD3B3C2C9FA5CB3B6CDF9B50C30";

exports.fhirServerUrl = "http://localhost:3000/3_0_1";

exports.cpabeServerUrl = "http://localhost:4000"

exports.abi = [{"constant":false,"inputs":[{"name":"downloadUrl","type":"string"},{"name":"doc","type":"address"},{"name":"accessExpiryTime","type":"uint256"}],"name":"giveAccess","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"doc","type":"address"}],"name":"logAccess","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"constant":false,"inputs":[{"name":"patient","type":"address"}],"name":"revokeAccess","outputs":[],"payable":false,"stateMutability":"nonpayable","type":"function"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_patient","type":"address"},{"indexed":true,"name":"_doctor","type":"address"},{"indexed":false,"name":"time","type":"uint256"},{"indexed":false,"name":"accessexpirytime","type":"uint256"}],"name":"accessGiven","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_patient","type":"address"},{"indexed":true,"name":"_doctor","type":"address"},{"indexed":false,"name":"time","type":"uint256"}],"name":"accessRevoked","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"name":"_patient","type":"address"},{"indexed":true,"name":"_doctor","type":"address"},{"indexed":false,"name":"time","type":"uint256"}],"name":"fileAccess","type":"event"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"accessExpiry","outputs":[{"name":"","type":"uint256"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"accessGivenDoctorAddress","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"accessGivenPatientAddress","outputs":[{"name":"","type":"address"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"","type":"address"}],"name":"fileUrl","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"},{"constant":true,"inputs":[{"name":"doc","type":"address"}],"name":"getUrl","outputs":[{"name":"","type":"string"}],"payable":false,"stateMutability":"view","type":"function"}];

exports.contractAddress = "0xc2ca1693245340a9267549d6c7117abef55493b1";