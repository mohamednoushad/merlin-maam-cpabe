pragma solidity ^0.5.8;

contract medChain
{
    mapping(address => string) fileUrl;
    mapping (address => address) accessGivenPatientAddress;
    mapping (address => address) accessGivenDoctorAddress;
    
    event accessGiven(
        address indexed _patient,
        address indexed _doctor,
        uint time
    );
    
      event accessRevoked(
        address indexed _patient,
        address indexed _doctor,
        uint time
    );
    
     event fileAccess(
        address indexed _patient,
        address indexed _doctor,
        uint time
    );

    function giveAccess (string memory downloadUrl, address doc) public
    {
        address patient = msg.sender;
        accessGivenPatientAddress[doc] = patient;
        accessGivenDoctorAddress[patient] = doc;
        fileUrl[doc] = downloadUrl;

        emit accessGiven(patient, doc,now);
    }
    
    function revokeAccess (address patient) public {
        
        address doc = accessGivenDoctorAddress[patient];
        fileUrl[doc] = '';
        accessGivenPatientAddress[doc] = 0x0000000000000000000000000000000000000000;
        accessGivenDoctorAddress[patient] = 0x0000000000000000000000000000000000000000;
        
         emit accessRevoked(patient, doc,now);
 
    }

    function getUrl(address doc) public view returns (string memory)
    {
        return fileUrl[doc];
    }
    
     function logAccess(address doc) public {
         
         address patient = accessGivenPatientAddress[doc];
        
         emit fileAccess(patient,doc,now);
         
     }
        
}

 
