pragma solidity ^0.5.8;

contract medChain
{
    mapping(address => string) public fileUrl;
    mapping (address => address) public accessGivenPatientAddress;
    mapping (address => address) public accessGivenDoctorAddress;
    mapping(address => uint) public accessExpiry;
    
    event accessGiven(
        address indexed _patient,
        address indexed _doctor,
        uint time,
        uint accessexpirytime
    );
    
      event accessRevoked(
        address indexed _patient,
        address indexed _doctor,
        uint time
    );
    
     event fileAccess(
        address indexed _patient,
        address indexed _doctor,
        uint time
    );

    function giveAccess (string memory downloadUrl, address doc, uint accessExpiryTime) public
    {
        address patient = msg.sender;
        accessGivenPatientAddress[doc] = patient;
        accessGivenDoctorAddress[patient] = doc;
        fileUrl[doc] = downloadUrl;
        accessExpiry[doc] = accessExpiryTime;
 
         emit accessGiven(patient, doc,now,accessExpiryTime);
    }
    
    function revokeAccess (address patient) public {
        
        address doc = accessGivenDoctorAddress[patient];
        fileUrl[doc] = '';
        accessGivenPatientAddress[doc] = 0x0000000000000000000000000000000000000000;
        accessGivenDoctorAddress[patient] = 0x0000000000000000000000000000000000000000;
        
        accessExpiry[doc] = 0;
        
        emit accessRevoked(patient, doc,now);
 
    }

    function getUrl(address doc) public view returns (string memory)
    {
        if(accessExpiry[doc] > now) {
            
            return fileUrl[doc];
            
        }
        
    }
    
     function logAccess(address doc) public {
         
         address patient = accessGivenPatientAddress[doc];
        
         emit fileAccess(patient,doc,now);
         
     }
        
}

 
