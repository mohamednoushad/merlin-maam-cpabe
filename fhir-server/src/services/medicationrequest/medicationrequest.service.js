/*eslint no-unused-vars: "warn"*/

const { RESOURCES } = require('@asymmetrik/node-fhir-server-core').constants;
const FHIRServer = require('@asymmetrik/node-fhir-server-core');
const globals = require('../../globals');
const { CLIENT } = require('../../constants');

let getMedicationRequest = (base_version) => {
	return require(FHIRServer.resolveFromVersion(base_version, RESOURCES.MEDICATIONREQUEST));};

let getMeta = (base_version) => {
	return require(FHIRServer.resolveFromVersion(base_version, RESOURCES.META));};

function mapMedicationRequestResultToMedicationRequestFHIRResource(medicationRequest){
    let fhirResource = {};
    fhirResource.identifier = [
        {use: 'usual', system: 'https://www.open-emr.org', value: medicationRequest.id },
    ];

    fhirResource.status = medicationRequest.active;
    fhirResource.intent = 'order';
    fhirResource.medicationCodeableConcept = {
        'coding': [
            {
                'system': 'http://snomed.info/sct',
                'code': medicationRequest.drug_code,
                'display': medicationRequest.drug
            }
        ],
        text: medicationRequest.drug
    };


    fhirResource.subject = {
        'id': medicationRequest.patient_id,
        'reference': `Patient/${medicationRequest.patient_id}`,
        'display': medicationRequest.patient_name
    };

    fhirResource.authoredOn = medicationRequest.date_added;

    fhirResource.requester = {
        'agent': {
            'id': medicationRequest.provider_id,
            'reference': `Practitioner/${medicationRequest.provider_id}`
        }
    };

    fhirResource.recorder = {
        'id': medicationRequest.provider_id,
        'reference': `Practitioner/${medicationRequest.provider_id}`,
        'display': `${medicationRequest.providerFirstName} ${medicationRequest.providerLastName}`
    };

    fhirResource.reasonCode = [
        {
            'coding': [
                {
                    'system': 'http://snomed.info/sct',
                    'code': medicationRequest.reason_code,
                    'display': medicationRequest.reason
                }
            ]
        }
    ];

    fhirResource.dosageInstruction = [{
        additionalInstruction: [
            {
                'coding': [
                    {
                        'system': 'http://snomed.info/sct',
                        'code': medicationRequest.additional_instruction_code,
                        'display': medicationRequest.additional_instruction
                    }
                ]
            }
        ],
        timing: { code: {'coding': [
        {
            'system': 'http://hl7.org/fhir/ValueSet/timing-abbreviation',
            'display': medicationRequest.drug_interval
        }
    ] },
            'repeat': { frequency: medicationRequest.dosage_timing_frequency, period: medicationRequest.dosage_timing_period, periodUnit: medicationRequest.dosage_timing_period_unit}},
        route: {
            'coding': [
                {
                    'system': 'http://snomed.info/sct',
                    'code': medicationRequest.drug_route_code,
                    'display': medicationRequest.drug_route_name
                }]
           },
        doseQuantity: {value: medicationRequest.dosage, unit: medicationRequest.drug_quantity_unit, system: 'http://hl7.org/fhir/v3/orderableDrugForm'}
    }];

    fhirResource.dispenseRequest = {
        quantity: {value: medicationRequest.dispense_quantity, unit: medicationRequest.dispense_quantity_unit, system: 'http://hl7.org/fhir/v3/orderableDrugForm'},
        'expectedSupplyDuration': {
            'value': medicationRequest.expected_supply_duration,
            'unit': medicationRequest.expected_supply_duration_unit,
            'system': 'http://unitsofmeasure.org'
        }
    };

    fhirResource.note = [{text: medicationRequest.note}];
    fhirResource.priority = medicationRequest.priority;

    fhirResource.meta = {versionId: 1, lastUpdated: medicationRequest.date_added};

    return fhirResource;
}


let buildStu3SearchQuery = (args) => {
    // Common search params
    let { base_version, _content, _format, _id, _lastUpdated, _profile, _query, _security, _tag } = args;

    // Search Result params
    let { _INCLUDE, _REVINCLUDE, _SORT, _COUNT, _SUMMARY, _ELEMENTS, _CONTAINED, _CONTAINEDTYPED } = args;

    // Resource Specific params
    let authoredon = args['authoredon'];
    let category = args['category'];
    let code = args['code'];
    let _context = args['_context'];
    let date = args['date'];
    let identifier = args['identifier'];
    let intended_dispenser = args['intended-dispenser'];
    let intent = args['intent'];
    let medication = args['medication'];
    let patient = args['patient'];
    let priority = args['priority'];
    let requester = args['requester'];
    let status = args['status'];
    let subject = args['subject'];

    let patientId;
    if (patient){
        let data = patient.split('/');
        patientId = data[1];
    }
    let query = `SELECT prescriptions.id,
		prescriptions.date_added ,
        prescriptions.patient_id,
        prescriptions.start_date,
        prescriptions.quantity,
        prescriptions.interval,
        prescriptions.note,
        prescriptions.drug,
        prescriptions.drug_id,
        prescriptions.drug_code,
        prescriptions.reason_code,
        prescriptions.reason,
        prescriptions.priority,
        prescriptions.medication,
        IF(prescriptions.active=1,'active','completed') AS active,
        prescriptions.provider_id,
        practioner.fname AS providerFirstName,
        practioner.lname AS providerLastName,
        prescriptions.size,
		prescriptions.rxnorm_drugcode,
        IFNULL(prescriptions.refills,0) AS refills,
        prescriptions.dosage,
        prescriptions.dosage_timing_code,
        prescriptions.dosage_timing_frequency,
        prescriptions.dosage_timing_period,
        prescriptions.dosage_timing_period_unit,
        prescriptions.additional_instruction_code,
        prescriptions.additional_instruction,
        prescriptions.drug_quantity_unit,
        prescriptions.expected_supply_duration,
        prescriptions.expected_supply_duration_unit,
        prescriptions.dispense_quantity,
        prescriptions.dispense_quantity_unit,
        lo2.title AS drug_form,
        lo2.codes AS drug_form_code,
        lo.title AS unit,
        lo.codes AS unit_code,
        lo3.title AS drug_interval,
         lo3.codes AS drug_interval_code,
          patient.genericname1 as patient_name,
        prescriptions.drug_code ,
        prescriptions.drug_route_name,
        prescriptions.drug_route_code
      FROM prescriptions 
      LEFT JOIN drugs AS drug ON prescriptions.drug_id = drug.drug_id
      LEFT JOIN list_options AS lo
      ON lo.list_id = 'drug_units' AND prescriptions.unit = lo.option_id AND lo.activity = 1
      LEFT JOIN list_options AS lo2
      ON lo2.list_id = 'drug_form' AND prescriptions.form = lo2.option_id AND lo2.activity = 1
        LEFT JOIN list_options AS lo3
      ON lo3.list_id = 'drug_interval' AND prescriptions.interval = lo3.option_id AND lo3.activity = 1
        LEFT JOIN patient_data as patient ON patient.pid = prescriptions.patient_id
        LEFT JOIN users AS practioner ON practioner.id = prescriptions.provider_id`;

    let params = [];
    let whereFilter = ' WHERE';
    if (patientId){
        whereFilter += ' prescriptions.patient_id = ? ';
        params.push(patientId);
    }

    let lastUpdated = args['_lastUpdated'];
    if (lastUpdated){
        if (params.length ) {
            whereFilter += ' AND';
        }
        let order = lastUpdated.substring(0, 2);
        if (order === 'lt'){
            whereFilter += ' prescriptions.date_added < ? ';
        }
        else {
            whereFilter += ' prescriptions.date_added > ? ';
        }

        let lastUpdatedDate = lastUpdated.substr(2);
        params.push(lastUpdatedDate);
    }

    if (params.length) {
        query += whereFilter;
    }

    let sort = args['_sort'];
    if (sort && sort === '_lastUpdated'){
        query += ' ORDER BY prescriptions.date_added';
    }

    return {query, params};

};

module.exports.search = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('MedicationRequest >>> search');

    let base_version = '3_0_1';
    let {query, params} = buildStu3SearchQuery(args);

	let MedicationRequest = getMedicationRequest(base_version);
    let connection = globals.get(CLIENT);
    connection.query(query, params, function (err, results, fields){
        if (err) {
            logger.error('Error with MedicationRequest.search: ', err);
            reject(err);
        }
        else {
            let response = [];
            if (results && results.length !== 0) {
                for (let data of results) {
                    let medicationRequestResult = mapMedicationRequestResultToMedicationRequestFHIRResource(data);
                    response.push(new MedicationRequest(medicationRequestResult));
                }
            }
            resolve(response);
        }
        resolve();
    });
});

function getMedicationRequestWithIdQuery(medicationRequestId) {
    let query = `SELECT prescriptions.id,
		prescriptions.date_added ,
        prescriptions.patient_id,
        prescriptions.start_date,
        prescriptions.quantity,
        prescriptions.interval,
        prescriptions.note,
        prescriptions.drug,
        prescriptions.drug_id,
        prescriptions.drug_code,
        prescriptions.reason_code,
        prescriptions.reason,
        prescriptions.priority,
        prescriptions.medication,
        IF(prescriptions.active=1,'active','completed') AS active,
        prescriptions.provider_id,
        practioner.fname AS providerFirstName,
        practioner.lname AS providerLastName,
        prescriptions.size,
		prescriptions.rxnorm_drugcode,
        IFNULL(prescriptions.refills,0) AS refills,
        prescriptions.dosage,
        prescriptions.dosage_timing_code,
        prescriptions.dosage_timing_frequency,
        prescriptions.dosage_timing_period,
        prescriptions.dosage_timing_period_unit,
        prescriptions.additional_instruction_code,
        prescriptions.additional_instruction,
        prescriptions.drug_quantity_unit,
        prescriptions.expected_supply_duration,
        prescriptions.expected_supply_duration_unit,
        prescriptions.dispense_quantity,
        prescriptions.dispense_quantity_unit,
        lo2.title AS drug_form,
        lo2.codes AS drug_form_code,
        lo.title AS unit,
        lo.codes AS unit_code,
        lo3.title AS drug_interval,
         lo3.codes AS drug_interval_code,
          patient.genericname1 as patient_name,
        prescriptions.drug_code ,
        prescriptions.drug_route_name,
        prescriptions.drug_route_code
      FROM prescriptions 
      LEFT JOIN drugs AS drug ON prescriptions.drug_id = drug.drug_id
      LEFT JOIN list_options AS lo
      ON lo.list_id = 'drug_units' AND prescriptions.unit = lo.option_id AND lo.activity = 1
      LEFT JOIN list_options AS lo2
      ON lo2.list_id = 'drug_form' AND prescriptions.form = lo2.option_id AND lo2.activity = 1
        LEFT JOIN list_options AS lo3
      ON lo3.list_id = 'drug_interval' AND prescriptions.interval = lo3.option_id AND lo3.activity = 1
        LEFT JOIN patient_data as patient ON patient.pid = prescriptions.patient_id
        LEFT JOIN users AS practioner ON practioner.id = prescriptions.provider_id
      WHERE prescriptions.id = ?;`;

    return {query, params: [medicationRequestId]};
}

module.exports.searchById = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('MedicationRequest >>> searchById');

	let { base_version, id } = args;

	let MedicationRequest = getMedicationRequest(base_version);

	let connection = globals.get(CLIENT);
    let {query, params} = getMedicationRequestWithIdQuery(id);
    connection.query(query, params, function (err, results, fields){
        if (err) {
            logger.error('Error with Observation.searchById: ', err);
            reject(err);
        }
        else {
            if (results[0]) {
                let medicationRequestResult = mapMedicationRequestResultToMedicationRequestFHIRResource(results[0]);
                resolve(new MedicationRequest(medicationRequestResult));
            }
        }
        resolve();
    });
});

module.exports.create = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('MedicationRequest >>> create');

	let { base_version, id, resource } = args;

	let MedicationRequest = getMedicationRequest(base_version);
	let Meta = getMeta(base_version);

	// TODO: determine if client/server sets ID

	// Cast resource to MedicationRequest Class
	let medicationrequest_resource = new MedicationRequest(resource);
	medicationrequest_resource.meta = new Meta();
	// TODO: set meta info

	// TODO: save record to database

	// Return Id
	resolve({ id: medicationrequest_resource.id });
});

module.exports.update = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('MedicationRequest >>> update');

	let { base_version, id, resource } = args;

	let MedicationRequest = getMedicationRequest(base_version);
	let Meta = getMeta(base_version);

	// Cast resource to MedicationRequest Class
	let medicationrequest_resource = new MedicationRequest(resource);
	medicationrequest_resource.meta = new Meta();
	// TODO: set meta info, increment meta ID

	// TODO: save record to database

	// Return id, if recorded was created or updated, new meta version id
	resolve({ id: medicationrequest_resource.id, created: false, resource_version: medicationrequest_resource.meta.versionId });
});

module.exports.remove = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('MedicationRequest >>> remove');

	let { id } = args;

	// TODO: delete record in database (soft/hard)

	// Return number of records deleted
	resolve({ deleted: 0 });
});

module.exports.searchByVersionId = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('MedicationRequest >>> searchByVersionId');

	let { base_version, id, version_id } = args;

	let MedicationRequest = getMedicationRequest(base_version);

	// TODO: Build query from Parameters

	// TODO: Query database

	// Cast result to MedicationRequest Class
	let medicationrequest_resource = new MedicationRequest();

	// Return resource class
	resolve(medicationrequest_resource);
});

module.exports.history = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('MedicationRequest >>> history');

	// Common search params
	let { base_version, _content, _format, _id, _lastUpdated, _profile, _query, _security, _tag } = args;

	// Search Result params
	let { _INCLUDE, _REVINCLUDE, _SORT, _COUNT, _SUMMARY, _ELEMENTS, _CONTAINED, _CONTAINEDTYPED } = args;

	// Resource Specific params
	let authoredon = args['authoredon'];
	let category = args['category'];
	let code = args['code'];
	let _context = args['_context'];
	let date = args['date'];
	let identifier = args['identifier'];
	let intended_dispenser = args['intended-dispenser'];
	let intent = args['intent'];
	let medication = args['medication'];
	let patient = args['patient'];
	let priority = args['priority'];
	let requester = args['requester'];
	let status = args['status'];
	let subject = args['subject'];

	// TODO: Build query from Parameters

	// TODO: Query database

	let MedicationRequest = getMedicationRequest(base_version);

	// Cast all results to MedicationRequest Class
	let medicationrequest_resource = new MedicationRequest();

	// Return Array
	resolve([medicationrequest_resource]);
});

module.exports.historyById = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('MedicationRequest >>> historyById');

	// Common search params
	let { base_version, _content, _format, _id, _lastUpdated, _profile, _query, _security, _tag } = args;

	// Search Result params
	let { _INCLUDE, _REVINCLUDE, _SORT, _COUNT, _SUMMARY, _ELEMENTS, _CONTAINED, _CONTAINEDTYPED } = args;

	// Resource Specific params
	let authoredon = args['authoredon'];
	let category = args['category'];
	let code = args['code'];
	let _context = args['_context'];
	let date = args['date'];
	let identifier = args['identifier'];
	let intended_dispenser = args['intended-dispenser'];
	let intent = args['intent'];
	let medication = args['medication'];
	let patient = args['patient'];
	let priority = args['priority'];
	let requester = args['requester'];
	let status = args['status'];
	let subject = args['subject'];

	// TODO: Build query from Parameters

	// TODO: Query database

	let MedicationRequest = getMedicationRequest(base_version);

	// Cast all results to MedicationRequest Class
	let medicationrequest_resource = new MedicationRequest();

	// Return Array
	resolve([medicationrequest_resource]);
});

