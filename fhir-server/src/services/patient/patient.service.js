/*eslint no-unused-vars: "warn"*/

const { RESOURCES, VERSIONS } = require('@asymmetrik/node-fhir-server-core').constants;
const FHIRServer = require('@asymmetrik/node-fhir-server-core');
const { COLLECTION, CLIENT_DB, CLIENT, BASE_URL} = require('../../constants');
const moment = require('moment-timezone');
const globals = require('../../globals');

const { stringQueryBuilder,
	tokenQueryBuilder,
	referenceQueryBuilder,
	addressQueryBuilder,
	nameQueryBuilder,
	dateQueryBuilder } = require('../../utils/querybuilder.util');



let getPatient = (base_version) => {
	return require(FHIRServer.resolveFromVersion(base_version, 'Patient'));};

let getMeta = (base_version) => {
	return require(FHIRServer.resolveFromVersion(base_version, 'Meta'));};

function mapPatientResultToPatientFHIRResource(patient){
    let fhirResource = {
        gender: patient.gender,
        birthDate: patient.birthDate,
		id: patient.Pid,
    };

    fhirResource.identifier = [
        {use: 'usual', system: 'https://www.open-emr.org', value: patient.Identifier }
    ];

    if (patient.Ss){
        fhirResource.identifier.push({use: 'usual', system: 'Social Security', value: patient.Ss});
    }

    if (patient.DriversLicense){
        fhirResource.identifier.push({use: 'usual', system: 'Drivers License', value: patient.DriversLicense});
    }

    fhirResource.name = [
        {
            'use': 'usual',
            'text': patient.name,
            'family': patient.Lname,
            'given': [
                patient.Fname, patient.Mname
            ],
            'prefix': [
                patient.Title
            ],
            'suffix': [
                patient.Title
            ]
        }
    ];

    fhirResource. telecom = [];
    if (patient.PhoneCell){
        fhirResource. telecom.push({system: 'phone', value: patient.PhoneCell, use: 'mobile'});
    }

    if (patient.Email){
        fhirResource. telecom.push({system: 'email', value: patient.Email, use: 'home'});
    }

    if (patient.PhoneHome){
        fhirResource. telecom.push({system: 'phone', value: patient.PhoneHome, use: 'home'});
    }

    if (patient.PhoneBiz){
        fhirResource. telecom.push({system: 'phone', value: patient.PhoneBiz, use: 'work'});
    }

    switch (patient.maritalStatus){
        case 'single':
            fhirResource.maritalStatus = {coding: [{ code: 'U', system: 'http://hl7.org/fhir/v3/MaritalStatus'}], text: 'unmarried'};
            break;
        case 'married':
            fhirResource.maritalStatus = {coding: [{code: 'M', system: 'http://hl7.org/fhir/v3/MaritalStatus'}], text: 'Married'};
            break;
        case 'divorced':
            fhirResource.maritalStatus = {coding: [{code: 'D', system: 'http://hl7.org/fhir/v3/MaritalStatus'}], text: 'Divorced'};
            break;
        case 'widowed':
            fhirResource.maritalStatus = {coding: [{code: 'W', system: 'http://hl7.org/fhir/v3/MaritalStatus'}], text: 'Widowed'};
            break;
        case 'separated':
            fhirResource.maritalStatus = {coding: [{code: 'L', system: 'http://hl7.org/fhir/v3/MaritalStatus'}], text: 'Legally Separated'};
            break;
        case 'domestic partner':
            fhirResource.maritalStatus = {coding: [{code: 'T', system: 'http://hl7.org/fhir/v3/MaritalStatus'}], text: 'Domestic partner'};
            break;
        default:
            fhirResource.maritalStatus = {coding: [{code: 'UNK', system: 'http://hl7.org/fhir/v3/NullFlavor'}], text: 'unknown'};

    }

    fhirResource.address = [{
        use: 'home',
        type: 'postal',
        text: `${patient.Street}, ${patient.PostalCode}`,
        line: [patient.Street, patient.PostalCode],
        city: patient.City,
        state: patient.State,
        postalCode: patient.PostalCode,
        country: patient.CountryCode

    }];

    let guardianRelationship;

    switch (patient.guardianRelationship){
        case 'brother':
        case 'child':
        case 'life partner':
        case 'friend':
        case 'father':
        case 'grandchild':
        case 'guardian':
        case 'grandparent':
        case 'mother':
        case 'natural child':
        case 'parent':
        case 'stepchild':
        case 'sibling':
        case 'sister':
        case 'spouse':
            guardianRelationship = {coding: [{ code: 'N', system: 'http://hl7.org/fhir/v2/0131'}], text: 'Next-of-Kin'};
            break;
        case 'emergency contact':
            guardianRelationship = {coding: [{ code: 'C', system: 'http://hl7.org/fhir/v2/0131'}], text: 'Emergency Contact'};
            break;
        case 'employer':
        case 'manager':
            guardianRelationship = {coding: [{ code: 'E', system: 'http://hl7.org/fhir/v2/0131'}], text: 'Employer'};
            break;
        case 'other':
        case 'assosiate':
        case 'care giver':
        case 'handicaped dependant':
        case 'employee':
        case 'owner':
        case 'trainer':
        case 'ward of court':
            guardianRelationship = {coding: [{ code: 'O', system: 'http://hl7.org/fhir/v2/0131'}], text: 'Other'};
            break;
        default:
            guardianRelationship = {coding: [{ code: 'U', system: 'http://hl7.org/fhir/v2/0131'}], text: 'unknown'};
            break;
    }
    fhirResource.contact = [
        {
            relationship: guardianRelationship,
            name: [
                {
                    'use': 'usual',
                    'text': patient.guardiansName
                }
            ],
            gender: patient.guardianSex,
            address: [{
                use: 'home',
                type: 'postal',
                text: `${patient.guardianAddress}, ${patient.guardianPostalcode}`,
                line: [patient.guardianAddress, patient.guardianPostalcode],
                city: patient.guardianCity,
                state: patient.guardianState,
                postalCode: patient.PostalCode,
                country: patient.guardianCountry

            }]

        }
    ];

    let telecom = [];

    if (patient.guardianEmail){
        telecom.push({system: 'email', value: patient.guardianEmail, use: 'home'});
    }

    if (patient.guardianPhone){
        telecom.push({system: 'phone', value: patient.guardianPhone, use: 'home'});
    }

    if (patient.guardianWorkphone){
        telecom.push({system: 'phone', value: patient.guardianWorkphone, use: 'work'});
    }

    fhirResource.contact.telecom = telecom;

    fhirResource.meta = {versionId: 1, lastUpdated: patient.lastUpdated};

    return fhirResource;
}

let buildStu3SearchQuery = (args) =>	 {

	// Common search params
	let { _content, _format, _id, _lastUpdated, _profile, _query, _security, _tag } = args;

	// Search Result params
	let { _INCLUDE, _REVINCLUDE, _SORT, _COUNT, _SUMMARY, _ELEMENTS, _CONTAINED, _CONTAINEDTYPED } = args;

	// Patient search params
	let id = args['id'];
	let active = args['active'];
	let address = args['address'];
	let address_city = args['address-city'];
	let address_country = args['address-country'];
	let address_postalcode = args['address-postalcode'];
	let address_state = args['address-state'];
	let address_use = args['address-use'];
	let animal_breed = args['animal-breed'];
	let animal_species = args['animal-species'];
	let birthdate = args['birthdate'];
	let death_date = args['death-date'];
	let deceased = args['deceased'];
	let email = args['email'];
	let family = args['family'];
	let gender = args['gender'];
	let general_practitioner = args['general-practitioner'];
	let given = args['given'];
	let identifier = args['identifier'];
	let language = args['language'];
	let link = args['link'];
	let name = args['name'];
	let organization = args['organization'];
	let phone = args['phone'];
	let phonetic = args['phonetic'];
    let telecom = args['telecom'];
    

    let query = `select patient_data.id as Id
			,patient_data.title as Title
			,patient_data.language as Language
			,patient_data.financial as Financial
			,patient_data.fname as Fname
			,patient_data.lname as Lname
			,patient_data.mname as Mname
			,patient_data.DOB as birthDate
			,patient_data.street as Street
			,patient_data.postal_code as PostalCode
			,patient_data.city as City
			,patient_data.state as State
			,patient_data.country_code as CountryCode
			,patient_data.drivers_license as DriversLicense
			,patient_data.ss as Ss
			,patient_data.occupation as Occupation
			,patient_data.phone_home as PhoneHome
			,patient_data.phone_biz as PhoneBiz
			,patient_data.phone_contact as PhoneContact
			,patient_data.phone_cell as PhoneCell
			,patient_data.status as maritalStatus
			,patient_data.contact_relationship as ContactRelationship
			,patient_data.date as lastUpdated
			,patient_data.sex as gender
			,patient_data.email as Email
			,patient_data.email_direct as EmailDirect
			,patient_data.pubpid as Pubpid
			,patient_data.pid as Pid
			,patient_data.identifier as Identifier
			,patient_data.genericname1 as name
			,patient_data.mothersname as Mothersname
			,patient_data.guardiansname as guardiansName
            ,patient_data.guardianrelationship as guardianRelationship
            ,patient_data.guardiansex as guardianSex
            ,patient_data.guardianaddress as guardianAddress
            ,patient_data.guardiancity as guardianCity
            ,patient_data.guardianstate as guardianState
            ,patient_data.guardianpostalcode as guardianPostalcode
            ,patient_data.guardiancountry as guardianCountry
            ,patient_data.guardianphone as guardianPhone
            ,patient_data.guardianworkphone as guardianWorkphone
             ,patient_data.guardianemail as guardianEmail
			,patient_data.providerID as generalPractitioner
			,patient_data.county as County
			,patient_data.industry as Industry
		from patient_data`;

    let params = [];
    if (identifier){
        query += ' WHERE patient_data.identifier = ? OR patient_data.pubpid = ?';
        params.push(identifier,identifier);
    }

	return {query, params};

};

/**
 *
 * @param {*} args
 * @param {*} context
 * @param {*} logger
 */
module.exports.search = (args, context, logger) => new Promise((resolve, reject) => {
    logger.info('Patient >>> search');

    let base_version = '3_0_1';
    let {query, params} = buildStu3SearchQuery(args);

    let connection = globals.get(CLIENT);
	let Patient = getPatient(base_version);

    connection.query(query, params, function (err, results, fields){
        if (err) {
            logger.error('Error with Patient.search: ', err);
            reject(err);
        }
        else {
            let response = [];
            if (results && results.length !== 0) {
                for (let data of results) {
                    let patientResult = mapPatientResultToPatientFHIRResource(data);
                    response.push(new Patient(patientResult));
                }
            }
            resolve(response);
        }
        resolve();
    });
});

function getPatientWithIdQuery(patientId){
    let query = `select patient_data.id as Id
			,patient_data.title as Title
			,patient_data.language as Language
			,patient_data.financial as Financial
			,patient_data.fname as Fname
			,patient_data.lname as Lname
			,patient_data.mname as Mname
			,patient_data.DOB as birthDate
			,patient_data.street as Street
			,patient_data.postal_code as PostalCode
			,patient_data.city as City
			,patient_data.state as State
			,patient_data.country_code as CountryCode
			,patient_data.drivers_license as DriversLicense
			,patient_data.ss as Ss
			,patient_data.occupation as Occupation
			,patient_data.phone_home as PhoneHome
			,patient_data.phone_biz as PhoneBiz
			,patient_data.phone_contact as PhoneContact
			,patient_data.phone_cell as PhoneCell
			,patient_data.status as maritalStatus
			,patient_data.contact_relationship as ContactRelationship
			,patient_data.date as lastUpdated
			,patient_data.sex as gender
			,patient_data.email as Email
			,patient_data.email_direct as EmailDirect
			,patient_data.pubpid as Pubpid
			,patient_data.pid as Pid
			,patient_data.genericname1 as name
			,patient_data.mothersname as Mothersname
			,patient_data.guardiansname as guardiansName
            ,patient_data.guardianrelationship as guardianRelationship
            ,patient_data.guardiansex as guardianSex
            ,patient_data.guardianaddress as guardianAddress
            ,patient_data.guardiancity as guardianCity
            ,patient_data.guardianstate as guardianState
            ,patient_data.guardianpostalcode as guardianPostalcode
            ,patient_data.guardiancountry as guardianCountry
            ,patient_data.guardianphone as guardianPhone
            ,patient_data.guardianworkphone as guardianWorkphone
             ,patient_data.guardianemail as guardianEmail
			,patient_data.providerID as generalPractitioner
			,patient_data.county as County
			,patient_data.industry as Industry
		from patient_data WHERE patient_data.pid = ?`;

    return {query, params: [patientId]};
}


module.exports.searchById = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Patient >>> searchById');

	let { base_version, id } = args;
	let Patient = getPatient(base_version);

	let connection = globals.get(CLIENT);
	let {query, params} = getPatientWithIdQuery(id);
    connection.query(query, params, function (err, results, fields){
        if (err) {
            logger.error('Error with Patient.searchById: ', err);
            reject(err);
        }
        else {
        	if (results[0]) {
        		let patientResult = mapPatientResultToPatientFHIRResource(results[0]);
                resolve(new Patient(patientResult));
            }
        }
        resolve();
    });
});

function createPatientQuery(patientDetails){
    let query = `INSERT INTO patient_data ( pid, identifier,
    fname, lname, sex, DOB, phone_cell, city, state, postal_code, country_code, ss, street) VALUES 
     (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)`;
    let ss = '';
    let identifier = '';
    if (patientDetails.identifier) {
        for (let data of patientDetails.identifier) {
            if (data.system === 'Social Security') {
                ss = data.value;
            }
            if (data.system === 'Old Identification Number'){
                identifier = data.value;
			}
        }
    }
	let cellPhone = '';
    if (patientDetails.telecom) {
        for (let data of patientDetails.telecom) {
            if (data.use === 'mobile') {
                cellPhone = data.value;
            }
        }
    }
    let given = '';
    let family = '';
    if (patientDetails.name) {
        for (let data of patientDetails.name) {
            if (data.use === 'usual') {
            	if (data.given && data.given.length !== 0){
                    given = data.given[0];
				}
                if (data.family && data.family.length !== 0){
                    family = data.family[0];
                }
            }
        }
    }
    let city = '';
    let state = '';
    let postalCode = '';
    let country = '';
    let street = '';
    if (patientDetails.address) {
        for (let data of patientDetails.address) {
            if (data.type === 'postal') {
                city = data.city;
                state = data.state;
                postalCode = data.postalCode;
                country = data.country;
                street = data.text;
            }
        }
    }
    let params = [patientDetails.pid, identifier, given, family,
	patientDetails.gender, patientDetails.birthDate, cellPhone, city,
	state, postalCode, country,
        ss, street];
    return {query, params};
}

module.exports.create = (args, context, logger) => new Promise(async (resolve, reject) => {
	logger.info('Patient >>> create');

	let { resource } = args;
    let base_version = '3_0_1';

    let Patient = getPatient(base_version);
    let Meta = getMeta(base_version);

    let patient_resource = new Patient(resource);
    patient_resource.meta = new Meta();

    let connection = globals.get(CLIENT);

    connection.query('SELECT MAX(pid) AS pid FROM patient_data', [], function (error, response) {
        if (error) {
            logger.error('Error with PatientHistory.create: ', error);
            return reject(error);
        }
        if (response && response.length){
            patient_resource.pid = response[0].pid + 1;
        }
        else {
            patient_resource.pid = 1;
        }

        let {query, params} = createPatientQuery(patient_resource);
        connection.query(query, params, function (err, res) {
            if (err) {
                logger.error('Error with PatientHistory.create: ', err);
                reject(err);
            }
            else {
                return resolve({id: res.insertId});
            }
            resolve();
        });
    });

});

function updatePatientQuery(id,patientDetails){

    let ss = '';
    let identifier = '';
    if (patientDetails.identifier) {
        for (let data of patientDetails.identifier) {
            if (data.system === 'Social Security') {
                ss = data.value;
            }
            if (data.system === 'Old Identification Number'){
                identifier = data.value;
            }
            if (data.system === 'https://www.open-emr.org'){
                identifier = data.value;
            }
            
        }
    }
	let cellPhone = '';
    if (patientDetails.telecom) {
        for (let data of patientDetails.telecom) {
            if (data.use === 'mobile') {
                cellPhone = data.value;
            }
        }
    }
    let given = '';
    let family = '';
    if (patientDetails.name) {
        for (let data of patientDetails.name) {
            if (data.use === 'usual') {
            	if (data.given && data.given.length !== 0){
                    given = data.given[0];
				}
                if (data.family && data.family.length !== 0){
                    family = data.family;
                }
            }
        }
    }
    let city = '';
    let state = '';
    let postalCode = '';
    let country = '';
    let street = '';
    if (patientDetails.address) {
        for (let data of patientDetails.address) {
            if (data.type === 'postal') {
                city = data.city;
                state = data.state;
                postalCode = data.postalCode;
                country = data.country;
                street = data.text;
            }
        }
    }
    
    let params = [identifier, given, family,patientDetails.gender, patientDetails.birthDate.split('T')[0], cellPhone, city,
	state, postalCode, country,ss, street];

    let columnNames = ['identifier','fname', 'lname', 'sex', 'DOB', 'phone_cell', 'city', 'state', 'postal_code', 'country_code', 'ss', 'street'];

    let query = `UPDATE patient_data SET ${columnNames.join('=?,')} =?  WHERE patient_data.pid = ${id} `;

    return {query, params};
}

module.exports.update = (args, context, logger) => new Promise((resolve, reject) => {

	logger.info('Patient >>> update');

    let {id, resource } = args;

    let base_version = '3_0_1';
    

	let Patient = getPatient(base_version);
    let Meta = getMeta(base_version);

    let patient_resource = new Patient(resource);
    patient_resource.meta = new Meta();

    let {query, params} = updatePatientQuery(id,patient_resource);

    let connection = globals.get(CLIENT);

    connection.query(query, params, function (err, res) {
        if (err) {
            logger.error('Error with Patient.update: ', err);
            reject(err);
        }
        else {
            return resolve({ id: String(res.insertId) });

        }
        resolve();
    });
});

module.exports.remove = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Patient >>> remove');

	let { base_version, id } = args;

	// Grab an instance of our DB and collection
	let db = globals.get(CLIENT_DB);
	let collection = db.collection(`${COLLECTION.PATIENT}_${base_version}`);
	// Delete our patient record
	collection.deleteOne({ id: id }, (err, _) => {
		if (err) {
			logger.error('Error with Patient.remove');
			return reject({
				// Must be 405 (Method Not Allowed) or 409 (Conflict)
				// 405 if you do not want to allow the delete
				// 409 if you can't delete because of referential
				// integrity or some other reason
				code: 409,
				message: err.message
			});
		}

		// delete history as well.  You can chose to save history.  Up to you
		let history_collection = db.collection(`${COLLECTION.PATIENT}_${base_version}_History`);
		return history_collection.deleteMany({ id: id }, (err2) => {
			if (err2) {
				logger.error('Error with Patient.remove');
				return reject({
					// Must be 405 (Method Not Allowed) or 409 (Conflict)
					// 405 if you do not want to allow the delete
					// 409 if you can't delete because of referential
					// integrity or some other reason
					code: 409,
					message: err2.message
				});
			}

			return resolve({ deleted: _.result && _.result.n });
		});

	});
});

module.exports.searchByVersionId = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Patient >>> searchByVersionId');

	let { base_version, id, version_id } = args;

	let Patient = getPatient(base_version);

	let db = globals.get(CLIENT_DB);
	let history_collection = db.collection(`${COLLECTION.PATIENT}_${base_version}_History`);
	// Query our collection for this observation
	history_collection.findOne({ id: id.toString(), 'meta.versionId': `${version_id}` }, (err, patient) => {
		if (err) {
			logger.error('Error with Patient.searchByVersionId: ', err);
			return reject(err);
		}

		if (patient) {
			resolve(new Patient(patient));
		}

		resolve();

	});
});

module.exports.history = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Patient >>> history');

	// Common search params
	let { base_version } = args;

	let query = {};

	if (base_version === VERSIONS['3_0_1']) {
		query = buildStu3SearchQuery(args);
	} else if (base_version === VERSIONS['1_0_2']) {
		query = buildDstu2SearchQuery(args);
	}

	// Grab an instance of our DB and collection
	let db = globals.get(CLIENT_DB);
	let history_collection = db.collection(`${COLLECTION.PATIENT}_${base_version}_History`);
	let Patient = getPatient(base_version);

	// Query our collection for this observation
	history_collection.find(query, (err, data) => {
		if (err) {
			logger.error('Error with Patient.history: ', err);
			return reject(err);
		}

		// Patient is a patient cursor, pull documents out before resolving
		data.toArray().then((patients) => {
			patients.forEach(function(element, i, returnArray) {
				returnArray[i] = new Patient(element);
			});
			resolve(patients);
		});
	});
});

module.exports.historyById = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Patient >>> historyById');

	let { base_version, id } = args;
	let query = {};

	if (base_version === VERSIONS['3_0_1']) {
		query = buildStu3SearchQuery(args);
	} else if (base_version === VERSIONS['1_0_2']) {
		//query = buildDstu2SearchQuery(args);
	}

	query.id = `${id}`;

	// Grab an instance of our DB and collection
	let db = globals.get(CLIENT_DB);
	let history_collection = db.collection(`${COLLECTION.PATIENT}_${base_version}_History`);
	let Patient = getPatient(base_version);

	// Query our collection for this observation
	history_collection.find(query, (err, data) => {
		if (err) {
			logger.error('Error with Patient.historyById: ', err);
			return reject(err);
		}

		// Patient is a patient cursor, pull documents out before resolving
		data.toArray().then((patients) => {
			patients.forEach(function(element, i, returnArray) {
				returnArray[i] = new Patient(element);
			});
			resolve(patients);
		});
	});
});

const path = require('path');
const encounterService = require(path.resolve('src/services/encounter/encounter.service.js'));
const diagnosticReportService = require(path.resolve('src/services/diagnosticreport/diagnosticreport.service.js'));
const imagingStudyService = require(path.resolve('src/services/imagingstudy/imagingstudy.service.js'));

module.exports.everythingById = (args, context, logger) => new Promise(async (resolve, reject) => {
    logger.info('Patient >>> everythingById');

    try {
        let {base_version, id, _sort, _lastUpdated} = args;
        let subject = `Patient/${id}`;


        let patient = await this.searchById({id, base_version}, context, logger);
        let results = {
            resourceType: 'Bundle',
            type: 'searchset',
            entry: [{fullUrl: `${BASE_URL}/Patient/${patient.id}`, resource: patient}]
        };

        let encounters = await encounterService.patientEncounters({
            base_version,
            subject,
            _sort,
            _lastUpdated
        }, context, logger);
        results.entry.push(...encounters);

        let diagnosticReports = await diagnosticReportService.getPatientDiagnosticReports({
            base_version,
            patient: subject,
            _sort,
            _lastUpdated
        }, context, logger);
        results.entry.push(...diagnosticReports);

        let imagingStudyReports = await imagingStudyService.patientImagingStudy({
            base_version,
            patient: subject,
            _sort,
            _lastUpdated
        }, context, logger);
        results.entry.push(...imagingStudyReports);
        resolve(results);
    } catch (err){
        reject(err);
    }

});


function patchPatientQuery(id,patchContent) {

    let columnNames = [];
    let params = [];

    if(patchContent.id ? patchContent.id : false) {
        columnNames.push('identifier');
        params.push(patchContent['id']);
    } 

    if(patchContent.name ? patchContent.name[0].given : false) {
        columnNames.push('fname');
        params.push(patchContent.name[0]['given']);
    }

    if(patchContent.name ? patchContent.name[0].family : false) {
        columnNames.push('lname');
        params.push(patchContent.name[0]['family']);
    }

    if(patchContent.gender ? patchContent.gender : false) {
        columnNames.push('sex');
        params.push(patchContent['gender']);
    }

    if(patchContent.birthDate ? patchContent.birthDate : false) {
        columnNames.push('DOB');
        params.push(patchContent['birthDate']);
    }

    if(patchContent.address ? patchContent.address[0].line : false) {
        columnNames.push('street');
        params.push(patchContent.address[0].line[0]+' ,'+patchContent.address[0].line[1]);
    }
    
    if(patchContent.address ? patchContent.address[0].city : false) {
        columnNames.push('city');
        params.push(patchContent.address[0]['city']);
    }

    if(patchContent.address ? patchContent.address[0].state : false) {
        columnNames.push('state');
        params.push(patchContent.address[0]['state']);
    }

    if(patchContent.address ? patchContent.address[0].postalCode : false) {
        columnNames.push('postal_code');
        params.push(patchContent.address[0]['postalCode']);
    }

    if(patchContent.address ? patchContent.address[0].country : false) {
        columnNames.push('country_code');
        params.push(patchContent.address[0]['country']);
    }

    let query = `UPDATE patient_data SET ${columnNames.join('=?,')} =? WHERE patient_data.pid = ${id}`;
 
    return {query, params};
}

module.exports.patch = (args, context, logger) => new Promise((resolve, reject) => {

    logger.info('Patient >>> PatchQuery');

    let {id, patchContent } = args;

    let {query, params} = patchPatientQuery(id,patchContent);

    let connection = globals.get(CLIENT);

    connection.query(query, params, function (err, results, fields){
        
        if (err) {
            logger.error('Error with Patient.PatchQuery: ', err);
            reject(err);
        }
        else if (results) {
                return resolve({id: String(results.insertId)});
            }
    });
       
});



