/*eslint no-unused-vars: "warn"*/

const { RESOURCES } = require('@asymmetrik/node-fhir-server-core').constants;
const FHIRServer = require('@asymmetrik/node-fhir-server-core');
const globals = require('../../globals');
const { CLIENT } = require('../../constants');

let getCondition = (base_version) => {
	return require(FHIRServer.resolveFromVersion(base_version, RESOURCES.CONDITION));};

let getMeta = (base_version) => {
	return require(FHIRServer.resolveFromVersion(base_version, RESOURCES.META));};


function buildStu3SearchQuery(args) {

	// Common search params
	let { base_version, _content, _format, _id, _lastUpdated, _profile, _query, _security, _tag } = args;

	// Search Result params
	let { _INCLUDE, _REVINCLUDE, _SORT, _COUNT, _SUMMARY, _ELEMENTS, _CONTAINED, _CONTAINEDTYPED } = args;

	// Resource Specific params
	let abatement_age = args['abatement-age'];
	let abatement_boolean = args['abatement-boolean'];
	let abatement_date = args['abatement-date'];
	let abatement_string = args['abatement-string'];
	let asserted_date = args['asserted-date'];
	let asserter = args['asserter'];
	let body_site = args['body-site'];
	let category = args['category'];
	let clinical_status = args['clinical-status'];
	let code = args['code'];
	let _context = args['_context'];
	let encounter = args['encounter'];
	let evidence = args['evidence'];
	let evidence_detail = args['evidence-detail'];
	let identifier = args['identifier'];
	let onset_age = args['onset-age'];
	let onset_date = args['onset-date'];
	let onset_info = args['onset-info'];
	let patient = args['patient'];
	let severity = args['severity'];
	let stage = args['stage'];
	let subject = args['subject'];
	let verification_status = args['verification-status'];

	let query = `SELECT lst.id AS problem_id,
				lst.date,
                lst.title AS problem,
                lst.begdate,
                lst.enddate,
                lst.diagnosis AS problem_code,
                lst.severity_al AS severity,
                lst.severity_code,
                lst.body_site_code,
                lst.body_site_name,
                lst.clinical_status,
                lst.symptom_code,
                lst.symptom_name,
                lst.verification_status,
                lst.comments,
                lst.pid,
				asserter.id AS asserterId, asserter.fname AS asserterFirstName,asserter.lname AS asserterLastName,
                encounter.encounter AS encounter
FROM lists AS lst
LEFT JOIN issue_encounter AS encounter ON encounter.list_id = lst.id
LEFT JOIN users AS asserter ON lst.user = asserter.id
 WHERE lst.type = ? `;

	let params = ['medical_problem'];

	let whereFilter = ' ';

	if (subject){
		let data = subject.split('/');
		let patientId = data[1];
		if (params.length) {
			whereFilter += ' AND';
		}
		whereFilter += ' lst.pid = ? ';
		params.push(+patientId);
	}

	let lastUpdated = args['_lastUpdated'];
	if (lastUpdated){
		if (params.length ) {
			whereFilter += ' AND';
		}
		let order = lastUpdated.substring(0, 2);
		if (order === 'lt'){
			whereFilter += ' lst.date < ? ';
		}
		else {
			whereFilter += ' lst.date > ? ';
		}

		let lastUpdatedDate = lastUpdated.substr(2);
		params.push(lastUpdatedDate);
	}

	if (params.length) {
		query += whereFilter;
	}

	let sort = args['_sort'];
	if (sort && sort === '_lastUpdated'){
		query += ' ORDER BY lst.date';
	}

	return {query, params};
}

function mapConditionResultToConditionFHIRResource(condition) {
	let fhirResource = {id: condition.problem_id};
	fhirResource.identifier = [
		{use: 'usual', system: 'https://www.open-emr.org', value: condition.problem_id },
	];
	fhirResource.clinicalStatus = condition.clinical_status;
	fhirResource.verificationStatus = condition.verification_status;

	fhirResource.category = [
		{
			'coding': [
				{
					'system': 'http://hl7.org/fhir/condition-category',
					'code': 'problem-list-item',
					'display': 'Problem List Item'
				}
			]
		}
	];

	fhirResource.code = {
		'coding': [
			{
				'system': 'http://hl7.org/fhir/sid/icd-10',
				'code': condition.problem_code,
				'display': condition.problem
			}
		]
	};

	fhirResource.context = {
		'id': condition.encounter,
		'reference': `Encounter/${condition.encounter}`
	};

	fhirResource.subject = {
		'id': condition.pid,
		'reference': `Patient/${condition.pid}`
	};

	fhirResource.onsetDateTime = condition.begdate;
	if (condition.enddate) {
		fhirResource.abatementDateTime = condition.enddate;
	}

	fhirResource.bodySite = [
		{
			'coding': [
				{
					'system': 'http://snomed.info/sct',
					'code': condition.body_site_code,
					'display': condition.body_site_name
				}
			]
		}
	];

	fhirResource.severity = {
		'coding': [
			{
				'system': 'http://snomed.info/sct',
				'code': condition.severity_code,
				'display': condition.severity
			}
		]
	};

	fhirResource.asserter = {
		'reference': `Practitioner/${condition.asserterId}`,
		'display': `${condition.asserterFirstName} ${condition.asserterLastName}`
	};


	fhirResource.evidence = [
		{
			'code': [
				{
					'coding': [
						{
							'system': 'http://snomed.info/sct',
							'code': condition.symptom_code,
							'display': condition.symptom_name
						}
					]
				}
			]
		}
	];

	fhirResource.note = [{text: condition.comments}];

	fhirResource.meta = {versionId: 1, lastUpdated: condition.date};

	return fhirResource;
}

module.exports.search = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Condition >>> search');


	let base_version = '3_0_1';
	let {query, params} = buildStu3SearchQuery(args);

	let Condition = getCondition(base_version);

	let connection = globals.get(CLIENT);

	connection.query(query, params, function (err, results, fields){
		if (err) {
			logger.error('Error with Condition.search: ', err);
			reject(err);
		}
		else {
			let response = [];
			if (results && results.length !== 0) {
				for (let data of results) {
					let conditionResult = mapConditionResultToConditionFHIRResource(data);
					response.push(new Condition(conditionResult));
				}
			}
			resolve(response);
		}
	});
});

function getConditionWithIdQuery(conditionId) {
    let query = `SELECT lst.id AS problem_id,
				lst.date,
                lst.title AS problem,
                lst.begdate,
                lst.enddate,
                lst.diagnosis AS problem_code,
                lst.severity_al AS severity,
                lst.severity_code,
                lst.body_site_code,
                lst.body_site_name,
                lst.clinical_status,
                lst.symptom_code,
                lst.symptom_name,
                lst.verification_status,
                lst.comments,
                lst.pid,
				asserter.id AS asserterId, asserter.fname AS asserterFirstName,asserter.lname AS asserterLastName,
                encounter.encounter AS encounter
FROM lists AS lst
LEFT JOIN issue_encounter AS encounter ON encounter.list_id = lst.id
LEFT JOIN users AS asserter ON lst.user = asserter.id
 WHERE lst.type = "medical_problem" AND lst.id= ?`;

    return {query, params: [conditionId]};
}

module.exports.searchById = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Condition >>> searchById');

	let { base_version, id } = args;

	let Condition = getCondition(base_version);

    let connection = globals.get(CLIENT);
    let {query, params} = getConditionWithIdQuery(id);
    connection.query(query, params, function (err, results, fields){
        if (err) {
            logger.error('Error with Observation.searchById: ', err);
            reject(err);
        }
        else {
            if (results[0]) {
                let conditionResult = mapConditionResultToConditionFHIRResource(results[0]);
                resolve(new Condition(conditionResult));
            }
        }
        resolve();
    });
});

module.exports.create = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Condition >>> create');

	let { base_version, id, resource } = args;

	let Condition = getCondition(base_version);
	let Meta = getMeta(base_version);

	// TODO: determine if client/server sets ID

	// Cast resource to Condition Class
	let condition_resource = new Condition(resource);
	condition_resource.meta = new Meta();
	// TODO: set meta info

	// TODO: save record to database

	// Return Id
	resolve({ id: condition_resource.id });
});

module.exports.update = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Condition >>> update');

	let { base_version, id, resource } = args;

	let Condition = getCondition(base_version);
	let Meta = getMeta(base_version);

	// Cast resource to Condition Class
	let condition_resource = new Condition(resource);
	condition_resource.meta = new Meta();
	// TODO: set meta info, increment meta ID

	// TODO: save record to database

	// Return id, if recorded was created or updated, new meta version id
	resolve({ id: condition_resource.id, created: false, resource_version: condition_resource.meta.versionId });
});

module.exports.remove = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Condition >>> remove');

	let { id } = args;

	// TODO: delete record in database (soft/hard)

	// Return number of records deleted
	resolve({ deleted: 0 });
});

module.exports.searchByVersionId = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Condition >>> searchByVersionId');

	let { base_version, id, version_id } = args;

	let Condition = getCondition(base_version);

	// TODO: Build query from Parameters

	// TODO: Query database

	// Cast result to Condition Class
	let condition_resource = new Condition();

	// Return resource class
	resolve(condition_resource);
});

module.exports.history = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Condition >>> history');

	// Common search params
	let { base_version, _content, _format, _id, _lastUpdated, _profile, _query, _security, _tag } = args;

	// Search Result params
	let { _INCLUDE, _REVINCLUDE, _SORT, _COUNT, _SUMMARY, _ELEMENTS, _CONTAINED, _CONTAINEDTYPED } = args;

	// Resource Specific params
	let abatement_age = args['abatement-age'];
	let abatement_boolean = args['abatement-boolean'];
	let abatement_date = args['abatement-date'];
	let abatement_string = args['abatement-string'];
	let asserted_date = args['asserted-date'];
	let asserter = args['asserter'];
	let body_site = args['body-site'];
	let category = args['category'];
	let clinical_status = args['clinical-status'];
	let code = args['code'];
	let _context = args['_context'];
	let encounter = args['encounter'];
	let evidence = args['evidence'];
	let evidence_detail = args['evidence-detail'];
	let identifier = args['identifier'];
	let onset_age = args['onset-age'];
	let onset_date = args['onset-date'];
	let onset_info = args['onset-info'];
	let patient = args['patient'];
	let severity = args['severity'];
	let stage = args['stage'];
	let subject = args['subject'];
	let verification_status = args['verification-status'];

	// TODO: Build query from Parameters

	// TODO: Query database

	let Condition = getCondition(base_version);

	// Cast all results to Condition Class
	let condition_resource = new Condition();

	// Return Array
	resolve([condition_resource]);
});

module.exports.historyById = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Condition >>> historyById');

	// Common search params
	let { base_version, _content, _format, _id, _lastUpdated, _profile, _query, _security, _tag } = args;

	// Search Result params
	let { _INCLUDE, _REVINCLUDE, _SORT, _COUNT, _SUMMARY, _ELEMENTS, _CONTAINED, _CONTAINEDTYPED } = args;

	// Resource Specific params
	let abatement_age = args['abatement-age'];
	let abatement_boolean = args['abatement-boolean'];
	let abatement_date = args['abatement-date'];
	let abatement_string = args['abatement-string'];
	let asserted_date = args['asserted-date'];
	let asserter = args['asserter'];
	let body_site = args['body-site'];
	let category = args['category'];
	let clinical_status = args['clinical-status'];
	let code = args['code'];
	let _context = args['_context'];
	let encounter = args['encounter'];
	let evidence = args['evidence'];
	let evidence_detail = args['evidence-detail'];
	let identifier = args['identifier'];
	let onset_age = args['onset-age'];
	let onset_date = args['onset-date'];
	let onset_info = args['onset-info'];
	let patient = args['patient'];
	let severity = args['severity'];
	let stage = args['stage'];
	let subject = args['subject'];
	let verification_status = args['verification-status'];

	// TODO: Build query from Parameters

	// TODO: Query database

	let Condition = getCondition(base_version);

	// Cast all results to Condition Class
	let condition_resource = new Condition();

	// Return Array
	resolve([condition_resource]);
});

