/*eslint no-unused-vars: "warn"*/

const { RESOURCES } = require('@asymmetrik/node-fhir-server-core').constants;
const FHIRServer = require('@asymmetrik/node-fhir-server-core');
const globals = require('../../globals');
const { CLIENT, BASE_URL, PACS_BASE_URL } = require('../../constants');

let getDiagnosticReport = (base_version) => {
	return require(FHIRServer.resolveFromVersion(base_version, RESOURCES.DIAGNOSTICREPORT));};

let getMeta = (base_version) => {
	return require(FHIRServer.resolveFromVersion(base_version, RESOURCES.META));};

function mapImagingStudy(study) {
		let fhirResource = {'resourceType': 'ImagingStudy'};
		fhirResource.id = study.id;
		fhirResource.uid = study.study_instance_uid;
		fhirResource.identifier = [
			{use: 'usual', system: 'https://www.open-emr.org', value: study.id },
		];

		fhirResource.patient = {
			'id': study.patientId,
			'reference': `Patient/${study.patientId}`
		};


		fhirResource. context = {
			'id': study.encounterId,
			'reference': `Encounter/${study.encounterId}`
		};

		fhirResource.availability = 'available';

		fhirResource.description = study.study_description;

		fhirResource.referrer =
			 {
					'reference': `Practitioner/${study.referrerId}`,
					'display': `${study.referrerFirstName} ${study.referrerLastName}`
				};


		fhirResource.interpreter = [
		{
			'reference': `Practitioner/${study.interpreterId}`,
			'display': `${study.interpreterFirstName} ${study.interpreterLastName}`
		}
		];


	fhirResource.started = study.study_date;

		fhirResource.procedureCode = [{
			'coding': [
				{
					'system': 'http://snomed.info/sct',
					'code': study.procedure_code,
					display: study.procedure_name
				}
			],
		}];

	fhirResource.reason = {
		'coding': [
			{
				'system': 'http://snomed.info/sct',
				'code': study.reasonCode,
				display: study.reason
			}
		],
	};

	fhirResource.description = study.study_description;
	fhirResource.numberOfSeries = 0;
		let studySeries = [];
		for (let seriesData of study.series){
			let series = {
				uid: seriesData.series_instance_id,
				modality:
					{
						'system': 'http://dicom.nema.org/resources/ontology/DCM',
						'code': seriesData.series_modaliity
					}
				,
				bodySite: {
					'system': 'http://snomed.info/sct',
					'code': study.bodySiteCode,
					'display': study.bodySite
				},
				description: seriesData.series_description,
				started: seriesData.series_date
			};

			series.endpoint = [];
			series.instance = [];
			series.numberOfInstances = 0;
			for (let instance of seriesData.instances){
				let instanceData = {
					uid: instance.instance_uid,
					number: instance.instance_number,
					sopClass: instance.sop_class_uid
				};
				series.endpoint.push({reference: `${PACS_BASE_URL}/dcm4chee-arc/aets/DCM4CHEE/wado?requestType=WADO&studyUID=${study.study_instance_uid}&seriesUID=${seriesData.series_instance_id}&objectUID=${instance.instance_uid}&contentType=application/dicom&transferSyntax=*`});
				series.instance.push(instanceData);
				series.numberOfInstances++;
			}
			fhirResource.numberOfSeries++;
			studySeries.push(series);
		}


	fhirResource.series = studySeries;


	return fhirResource;
}

function mapImagingStudyResultToDiagnosticReportFHIRResource(imagingStudy, base_version){
	let DiagnosticReport = getDiagnosticReport(base_version);
	let fhirResource = new DiagnosticReport();
	fhirResource.id = imagingStudy.id;
	fhirResource.identifier = [
		{use: 'usual', system: 'https://www.open-emr.org', value: imagingStudy.id },
	];

	fhirResource.category = {
			'coding': [
				{
					'system': 'http://snomed.info/sct',
					'code': '394914008',
					'display': 'Radiology'
				}
			]
		};

	fhirResource.code = {
		'coding': [
			{
				'code': imagingStudy.procedure_code,
				'display': imagingStudy.procedure_name
			}
		],
			'text': imagingStudy.procedure_name
	};
	fhirResource.subject = {
		'id': imagingStudy.patientId,
		'reference': `Patient/${imagingStudy.patientId}`
	};

	fhirResource.context = {
		'id': imagingStudy.encounterId,
		'reference': `Encounter/${imagingStudy.encounterId}`
	};
	fhirResource.performer = [
		{
			actor: {
			'reference': `Practitioner/${imagingStudy.referrerId}`,
			'display': `${imagingStudy.referrerFirstName} ${imagingStudy.referrerLastName}`
		}
		}
	];
	fhirResource.issued = imagingStudy.study_date;

	fhirResource.conclusion = imagingStudy.conclusion;

	let codedDiagnosis = [];

	for (let diagnosis of imagingStudy.conclusionCodes){
		let [system, code] = diagnosis.conclusion_code.split(':');
		let systemUrl;
		if (system === 'SNOMED-CT'){
			systemUrl = 'http://snomed.info/sct';
		}
		else if (system === 'LOINC'){
			systemUrl = 'http://loinc.org';
		}
		else {
			systemUrl = '';
		}

		codedDiagnosis.push({
			coding: [
				{
					'system': systemUrl,
					'code': code,
					'display': diagnosis.conclusion_code_term
				}
			]
		});
	}

	fhirResource.codedDiagnosis = codedDiagnosis;

	fhirResource.imagingStudy = {
		'id': imagingStudy.id,
		'reference': `#${imagingStudy.id}`
	};

	let study = mapImagingStudy(imagingStudy);
	let contained = [study];
	fhirResource.contained = contained;

	fhirResource.meta = {versionId: 1, lastUpdated: imagingStudy.date_ordered};
	return fhirResource;
}

let buildImagingStudyStu3SearchQuery = (args) => {
	// Common search params
	let { base_version, _content, _format, _id, _lastUpdated, _profile, _query, _security, _tag } = args;

	// Search Result params
	let { _INCLUDE, _REVINCLUDE, _SORT, _COUNT, _SUMMARY, _ELEMENTS, _CONTAINED, _CONTAINEDTYPED } = args;

	// Resource Specific params
	let based_on = args['based-on'];
	let category = args['category'];
	let code = args['code'];
	let _context = args['_context'];
	let date = args['date'];
	let diagnosis = args['diagnosis'];
	let encounter = args['encounter'];
	let identifier = args['identifier'];
	let image = args['image'];
	let issued = args['issued'];
	let patient = args['patient'];
	let performer = args['performer'];
	let result = args['result'];
	let specimen = args['specimen'];
	let status = args['status'];
	let subject = args['subject'];


	let query = `SELECT imaging_study.id, study_instance_uid, study_date, study_time, study_description, series_instance_id, series_modaliity, series_description, series_date, series_time, instance_uid, instance_number, series_body_site,
imaging_study.patientId, imaging_study.accession_number, imaging_study.bodySiteCode, imaging_study.bodySite, imaging_study.encounterId, imaging_study.conclusion,
imaging_study_conclusion_codes.codeId AS conclusion_code_id, imaging_study_conclusion_codes.conclusion_code, imaging_study_conclusion_codes.conclusion_code_term,
imaging_order_code.procedure_code, imaging_order_code.procedure_name, imaging_order_code.diagnoses AS reasonCode, imaging_order_code.reason,
referrer.id AS referrerId, referrer.fname AS referrerFirstName,referrer.lname AS referrerLastName,
interpreter.id AS interpreterId, interpreter.fname AS interpreterFirstName,interpreter.lname AS interpreterLastName
FROM openemr.imaging_study_instances JOIN imaging_study ON imaging_study_instances.study_instance_uid = imaging_study.studyId
JOIN imaging_order ON imaging_study.imaging_order_id = imaging_order.imaging_order_id
LEFT JOIN imaging_order_code ON imaging_order_code.imaging_order_id = imaging_order.imaging_order_id
LEFT JOIN imaging_study_conclusion_codes ON imaging_study_conclusion_codes.study_id = imaging_study.id
LEFT JOIN users AS referrer ON imaging_order.provider_id = referrer.id
LEFT JOIN users AS interpreter ON imaging_order.provider_id = interpreter.id
`;

	let params = [];

	let patientId;
	if (patient){
		let data = patient.split('/');
		patientId = data[1];
	}

	let whereFilter = ' WHERE';
	if (patientId){
		whereFilter += ' imaging_study.review_status = 1 AND imaging_study.patientId = ? ';
		params.push(patientId);
	}

	let lastUpdated = args['_lastUpdated'];
	if (lastUpdated){
		if (params.length ) {
			whereFilter += ' AND';
		}
		let order = lastUpdated.substring(0, 2);
		if (order === 'lt'){
			whereFilter += ' imaging_order.date_ordered < ? ';
		}
		else {
			whereFilter += ' imaging_order.date_ordered > ? ';
		}

		let lastUpdatedDate = lastUpdated.substr(2);
		params.push(lastUpdatedDate);
	}

	if (params.length) {
		query += whereFilter;
	}

	let sort = args['_sort'];
	if (sort && sort === '_lastUpdated'){
		query += ' ORDER BY imaging_order.date_ordered';
	}

	return {query, params};
};

//group imaging study instances into study and series
function groupImagingStudyInstances(imagingDiagnosticResults){
	let response = [];
	//group instances by study id as key
	if (imagingDiagnosticResults && imagingDiagnosticResults.length !== 0) {
		let groupedStudies = imagingDiagnosticResults.reduce(function (r, a) {
			r[a.id] = r[a.id] || [];
			r[a.id].push(a);
			return r;
		}, Object.create(null));

		for (let key in groupedStudies) {
			let studyInstances = groupedStudies[key];
			let studyDetails;
			if (studyInstances && studyInstances.length !== 0) {
				studyDetails = {
					id: studyInstances[0].id,
					study_instance_uid: studyInstances[0].study_instance_uid,
					study_date: studyInstances[0].study_date,
					study_time: studyInstances[0].study_time,
					study_description: studyInstances[0].study_description,
					patientId: studyInstances[0].patientId,
					accession_number: studyInstances[0].accession_number,
					bodySiteCode: studyInstances[0].bodySiteCode,
					bodySite: studyInstances[0].bodySite,
					encounterId: studyInstances[0].encounterId,
					conclusion: studyInstances[0].conclusion,
					procedure_code: studyInstances[0].procedure_code,
					procedure_name: studyInstances[0].procedure_name,
					reasonCode: studyInstances[0].reasonCode,
					reason: studyInstances[0].reason,
					referrerId: studyInstances[0].referrerId,
					referrerFirstName: studyInstances[0].referrerFirstName,
					referrerLastName: studyInstances[0].referrerLastName,
					interpreterId: studyInstances[0].interpreterId,
					interpreterFirstName: studyInstances[0].interpreterFirstName,
					interpreterLastName: studyInstances[0].interpreterLastName
				};
				studyDetails.series = [];
				studyDetails.conclusionCodes = [];
				//group instances within a study into resries by series id key
				let groupedSeries = studyInstances.reduce(function (r, a) {
					r[a.series_instance_id] = r[a.series_instance_id] || [];
					r[a.series_instance_id].push(a);
					return r;
				}, Object.create(null));
				for (let skey in groupedSeries) {
					let seriesInstances = groupedSeries[skey];
					if (seriesInstances && seriesInstances.length !== 0) {
						let seriesDetails = {
							series_instance_id: seriesInstances[0].series_instance_id,
							series_modaliity: seriesInstances[0].series_modaliity,
							series_description: seriesInstances[0].series_description,
							series_date: seriesInstances[0].series_date,
							series_time: seriesInstances[0].series_time,
							series_body_site: seriesInstances[0].series_body_site
						};

						let instances = [];
						for (let instance of seriesInstances){
							let hasInstance = instances.find(y => y.instance_uid === instance.instance_uid);
							if (!hasInstance) {
								let instance_details = {
									instance_uid: instance.instance_uid,
									instance_number: instance.instance_number,
									sop_class_uid: instance.sop_class_uid
								};

								instances.push(instance_details);
							}

							let hasConcCode = studyDetails.conclusionCodes.find(p => p.conclusion_code_id === instance.conclusion_code_id);
							if (!hasConcCode){
								let conclusionCode = {
									conclusion_code_id: instance.conclusion_code_id,
									conclusion_code: instance.conclusion_code,
									conclusion_code_term: instance.conclusion_code_term
								};
								studyDetails.conclusionCodes.push(conclusionCode);
							}
						}

						seriesDetails.instances = instances;
						let hasSeries = studyDetails.series.find(z => z.series_instance_id === seriesDetails.series_instance_id);
						if (!hasSeries){
							studyDetails.series.push(seriesDetails);
						}

					}

				}

			}
			let imagingStudyResult = mapImagingStudyResultToDiagnosticReportFHIRResource(studyDetails, '3_0_1');
			response.push(imagingStudyResult);
		}
	}

	return response;
}

function getImagingStudyReport(args, context, logger){
	return new Promise((resolve, reject) => {
		let connection = globals.get(CLIENT);
		let {query, params} = buildImagingStudyStu3SearchQuery(args);
		connection.query(query, params, function (err, imagingDiagnosticResults, fields){
			if (err) {
				logger.error('Error with DiagnosticReport.search: ', err);
				reject(err);
			}
			else {
				let response = groupImagingStudyInstances(imagingDiagnosticResults);
				resolve(response);
			}
			resolve();
		});
});
}


function mapObservations(results){
	let observations = [];

	for (let observation of results) {
		let [low, high] = observation.range.split('-');
		let obs = {
			'resourceType': 'Observation',
			'id': observation.procedure_result_id,
			'status': observation.result_status,
			'code': {
				'coding': [
					{
						'system': 'http://loinc.org',
						'code': observation.result_code,
						'display': observation.result_text
					}
				],
				'text': observation.result_text
			},
			'category': [{
				'coding': [
					{
						'system': 'http://hl7.org/fhir/observation-category',
						'code': 'laboratory',
						'display': 'Laboratory'
					}
				]
			}],
			'valueQuantity': {
				'value': observation.result,
				'unit': observation.units,
				'system': 'http://unitsofmeasure.org',
				'code': observation.units
			},
			'referenceRange': [
				{
					'low': {
						'value': low,
						'unit': observation.units,
						'system': 'http://unitsofmeasure.org',
						'code': observation.units
					},
					'high': {
						'value': high,
						'unit': observation.units,
						'system': 'http://unitsofmeasure.org',
						'code': observation.units
					}
				}
			]
		};
		observations.push(obs);
	}

	return observations;
}

function mapProcedureReportResultToDiagnosticReportFHIRResource(procedureReport, base_version){
	let DiagnosticReport = getDiagnosticReport(base_version);
	let fhirResource = new DiagnosticReport();
	fhirResource.id = procedureReport.procedure_report_id;
	fhirResource.identifier = [
		{use: 'usual', system: 'https://www.open-emr.org', value: procedureReport.procedure_report_id },
	];

	fhirResource.category = {
		'coding': [
			{
				'system': 'http://snomed.info/sct',
				'code': '15220000',
				'display': 'Laboratory test'
			}
		]
	};

	fhirResource.code = {
		'coding': [
			{
				'system': 'http://loinc.org',
				'code': procedureReport.procedure_code,
				'display': procedureReport.procedure_name
			}
		],
		'text': procedureReport.procedure_name
	};
	fhirResource.subject = {
		'id': procedureReport.patient_id,
		'reference': `Patient/${procedureReport.patient_id}`
	};

	fhirResource.context = {
		'id': procedureReport.encounter_id,
		'reference': `Encounter/${procedureReport.encounter_id}`
	};
	fhirResource.resultsInterpreter = [
		{
			actor: {
				'reference': `Practitioner/${procedureReport.provider_id}`,
				'display': `${procedureReport.providerFirstName} ${procedureReport.providerLastName}`
			}
		}
	];

	fhirResource.conclusion = procedureReport.conclusion;

	let codedDiagnosis = [];

	for (let diagnosis of procedureReport.conclusionCodes){
		let [system, code] = diagnosis.conclusion_code.split(':');
		let systemUrl;
		if (system === 'SNOMED-CT'){
			systemUrl = 'http://snomed.info/sct';
		}
		else if (system === 'LOINC'){
			systemUrl = 'http://loinc.org';
		}
		else {
			systemUrl = '';
		}

		codedDiagnosis.push({
			coding: [
				{
					'system': systemUrl,
					'code': code,
					'display': diagnosis.conclusion_code_term
				}
			]
		});
	}

	fhirResource.codedDiagnosis = codedDiagnosis;
	let contained = mapObservations(procedureReport.results);
	fhirResource.contained = contained;

	let observationResult = [];

	for (let result of procedureReport.results){
		observationResult.push({
			id: result.procedure_result_id,
			'reference': `#${result.procedure_result_id}`
		});
	}

	fhirResource.specimen = [
        {
            'display': procedureReport.specimen_name
        }];

	fhirResource.basedOn = [
		{
			'reference': `ProcedureRequest/${procedureReport.procedure_order_id}`
		}
	];
	fhirResource.issued = procedureReport.date_report;

	fhirResource.result = observationResult;

	fhirResource.status = procedureReport.result_status;

	fhirResource.meta = {versionId: 1, lastUpdated: procedureReport.result_date};
	return fhirResource;
}

let buildProcedureReportStu3SearchQuery = (args) => {
	// Common search params
	let { base_version, _content, _format, _id, _lastUpdated, _profile, _query, _security, _tag } = args;

	// Search Result params
	let { _INCLUDE, _REVINCLUDE, _SORT, _COUNT, _SUMMARY, _ELEMENTS, _CONTAINED, _CONTAINEDTYPED } = args;

	// Resource Specific params
	let based_on = args['based-on'];
	let category = args['category'];
	let code = args['code'];
	let _context = args['_context'];
	let date = args['date'];
	let diagnosis = args['diagnosis'];
	let encounter = args['encounter'];
	let identifier = args['identifier'];
	let image = args['image'];
	let issued = args['issued'];
	let patient = args['patient'];
	let performer = args['performer'];
	let result = args['result'];
	let specimen = args['specimen'];
	let status = args['status'];
	let subject = args['subject'];


	let query = `SELECT  procedure_report.procedure_report_id,procedure_report.procedure_order_id,date_report,conclusion, 
procedure_conclusion_codes.codeId AS conclusion_code_id, procedure_conclusion_codes.conclusion_code, procedure_conclusion_codes.conclusion_code_term, 
procedure_order.patient_id,procedure_order.provider_id,procedure_order.date_ordered,procedure_order.encounter_id,
procedure_order_code.procedure_code,procedure_order_code.procedure_name,procedure_order_code.specimen_code,procedure_order_code.specimen_name,
procedure_result_id,result_text,procedure_result.date AS result_date,units,result,result_code,procedure_result.range,abnormal,result_status FROM openemr.procedure_result 
JOIN procedure_report ON procedure_result.procedure_report_id = procedure_report.procedure_report_id
join procedure_conclusion_codes ON procedure_conclusion_codes.procedure_report_id = procedure_report.procedure_report_id
JOIN procedure_order ON procedure_report.procedure_order_id = procedure_order.procedure_order_id
JOIN procedure_order_code ON procedure_order.procedure_order_id = procedure_order_code.procedure_order_id
`;

	let params = [];

	let patientId;
	if (patient){
		let data = patient.split('/');
		patientId = data[1];
	}

	let whereFilter = ' WHERE';
	if (patientId){
		whereFilter += ' procedure_order.patient_id = ? ';
		params.push(patientId);
	}

	let lastUpdated = args['_lastUpdated'];
	if (lastUpdated){
		if (params.length ) {
			whereFilter += ' AND';
		}
		let order = lastUpdated.substring(0, 2);
		if (order === 'lt'){
			whereFilter += ' procedure_result.date < ? ';
		}
		else {
			whereFilter += ' procedure_result.date > ? ';
		}

		let lastUpdatedDate = lastUpdated.substr(2);
		params.push(lastUpdatedDate);
	}

	if (params.length) {
		query += whereFilter;
	}

	let sort = args['_sort'];
	if (sort && sort === '_lastUpdated'){
		query += ' ORDER BY procedure_result.date';
	}

	return {query, params};
};

function groupProcedureResults(procedureReportResults) {
	let procedureReports = [];
	if (procedureReportResults && procedureReportResults.length !== 0) {
		let results = procedureReportResults.reduce(function (r, a) {
			r[a.procedure_report_id] = r[a.procedure_report_id] || [];
			r[a.procedure_report_id].push(a);
			return r;
		}, Object.create(null));

		for (let key in results) {
			let value = results[key];
			let data;
			if (value && value.length !== 0) {
				data = value[0];
				data.results = [];
				data.conclusionCodes = [];
				for (let observation of value) {
					let hasObs = data.results.find(x => x.procedure_result_id === observation.procedure_result_id);
					if (!hasObs) {
						let procedure_observation = {
							procedure_result_id: observation.procedure_result_id,
							result_code: observation.result_code,
							result_text: observation.result_text,
							result_date: observation.result_date,
							units: observation.units,
							result: observation.result,
							range: observation.range,
							abnormal: observation.abnormal,
							result_status: observation.result_status
						};
						data.results.push(procedure_observation);
					}

					let hasConcCode = data.conclusionCodes.find(x => x.conclusion_code_id === observation.conclusion_code_id);
					if (!hasConcCode) {
						let conclusionCode = {
							conclusion_code_id: observation.conclusion_code_id,
							conclusion_code: observation.conclusion_code,
							conclusion_code_term: observation.conclusion_code_term
						};
						data.conclusionCodes.push(conclusionCode);
					}
				}
			}

			let procedureReportResult = mapProcedureReportResultToDiagnosticReportFHIRResource(data, '3_0_1');
			procedureReports.push(procedureReportResult);
		}

	}

	return procedureReports;
}

function getProcedureReport(args, context, logger){
	return new Promise((resolve, reject) => {
		let connection = globals.get(CLIENT);
		let {query, params} = buildProcedureReportStu3SearchQuery(args);
		connection.query(query, params, function (err, procedureReportResults, fields){
			if (err) {
				logger.error('Error with DiagnosticReport.search: ', err);
				reject(err);
			}
			else {
				let response = groupProcedureResults(procedureReportResults);
				resolve(response);
			}
			resolve();
		});
	});
}

module.exports.search = (args, context, logger) => new Promise(async(resolve, reject) => {
	logger.info('DiagnosticReport >>> search');
	try {
		let { base_version} = args;
		let diagnosticReports = [];
		let imagingDiagnosticResults = await getImagingStudyReport(args, context, logger);
		if (imagingDiagnosticResults && imagingDiagnosticResults.length !== 0) {
			diagnosticReports.push(...imagingDiagnosticResults);
		}
		let procedureReportResults = await getProcedureReport(args, context, logger);
		if (procedureReportResults && procedureReportResults.length !== 0) {
			diagnosticReports.push(...procedureReportResults);
		}

		resolve(diagnosticReports);
	} catch (err){
		reject(err);
	}
});

module.exports.searchById = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('DiagnosticReport >>> searchById');

	let { base_version, id } = args;

	let DiagnosticReport = getDiagnosticReport(base_version);

	// TODO: Build query from Parameters

	// TODO: Query database

	// Cast result to DiagnosticReport Class
	let diagnosticreport_resource = new DiagnosticReport();
	// TODO: Set data with constructor or setter methods
	diagnosticreport_resource.id = 'test id';

	// Return resource class
	// resolve(diagnosticreport_resource);
	resolve();
});

module.exports.create = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('DiagnosticReport >>> create');

	let { base_version, id, resource } = args;

	let DiagnosticReport = getDiagnosticReport(base_version);
	let Meta = getMeta(base_version);

	// TODO: determine if client/server sets ID

	// Cast resource to DiagnosticReport Class
	let diagnosticreport_resource = new DiagnosticReport(resource);
	diagnosticreport_resource.meta = new Meta();
	// TODO: set meta info

	// TODO: save record to database

	// Return Id
	resolve({ id: diagnosticreport_resource.id });
});

module.exports.update = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('DiagnosticReport >>> update');

	let { base_version, id, resource } = args;

	let DiagnosticReport = getDiagnosticReport(base_version);
	let Meta = getMeta(base_version);

	// Cast resource to DiagnosticReport Class
	let diagnosticreport_resource = new DiagnosticReport(resource);
	diagnosticreport_resource.meta = new Meta();
	// TODO: set meta info, increment meta ID

	// TODO: save record to database

	// Return id, if recorded was created or updated, new meta version id
	resolve({ id: diagnosticreport_resource.id, created: false, resource_version: diagnosticreport_resource.meta.versionId });
});

module.exports.remove = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('DiagnosticReport >>> remove');

	let { id } = args;

	// TODO: delete record in database (soft/hard)

	// Return number of records deleted
	resolve({ deleted: 0 });
});

module.exports.searchByVersionId = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('DiagnosticReport >>> searchByVersionId');

	let { base_version, id, version_id } = args;

	let DiagnosticReport = getDiagnosticReport(base_version);

	// TODO: Build query from Parameters

	// TODO: Query database

	// Cast result to DiagnosticReport Class
	let diagnosticreport_resource = new DiagnosticReport();

	// Return resource class
	resolve(diagnosticreport_resource);
});

module.exports.history = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('DiagnosticReport >>> history');

	// Common search params
	let { base_version, _content, _format, _id, _lastUpdated, _profile, _query, _security, _tag } = args;

	// Search Result params
	let { _INCLUDE, _REVINCLUDE, _SORT, _COUNT, _SUMMARY, _ELEMENTS, _CONTAINED, _CONTAINEDTYPED } = args;

	// Resource Specific params
	let based_on = args['based-on'];
	let category = args['category'];
	let code = args['code'];
	let _context = args['_context'];
	let date = args['date'];
	let diagnosis = args['diagnosis'];
	let encounter = args['encounter'];
	let identifier = args['identifier'];
	let image = args['image'];
	let issued = args['issued'];
	let patient = args['patient'];
	let performer = args['performer'];
	let result = args['result'];
	let specimen = args['specimen'];
	let status = args['status'];
	let subject = args['subject'];

	// TODO: Build query from Parameters

	// TODO: Query database

	let DiagnosticReport = getDiagnosticReport(base_version);

	// Cast all results to DiagnosticReport Class
	let diagnosticreport_resource = new DiagnosticReport();

	// Return Array
	resolve([diagnosticreport_resource]);
});

module.exports.historyById = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('DiagnosticReport >>> historyById');

	// Common search params
	let { base_version, _content, _format, _id, _lastUpdated, _profile, _query, _security, _tag } = args;

	// Search Result params
	let { _INCLUDE, _REVINCLUDE, _SORT, _COUNT, _SUMMARY, _ELEMENTS, _CONTAINED, _CONTAINEDTYPED } = args;

	// Resource Specific params
	let based_on = args['based-on'];
	let category = args['category'];
	let code = args['code'];
	let _context = args['_context'];
	let date = args['date'];
	let diagnosis = args['diagnosis'];
	let encounter = args['encounter'];
	let identifier = args['identifier'];
	let image = args['image'];
	let issued = args['issued'];
	let patient = args['patient'];
	let performer = args['performer'];
	let result = args['result'];
	let specimen = args['specimen'];
	let status = args['status'];
	let subject = args['subject'];

	// TODO: Build query from Parameters

	// TODO: Query database

	let DiagnosticReport = getDiagnosticReport(base_version);

	// Cast all results to DiagnosticReport Class
	let diagnosticreport_resource = new DiagnosticReport();

	// Return Array
	resolve([diagnosticreport_resource]);
});

// for evrything api in patient
module.exports.getPatientDiagnosticReports = (args, context, logger) => new Promise(async(resolve, reject) => {
	logger.info('DiagnosticReport >>> search');
	try {

		let { base_version} = args;
		let diagnosticReports = [];
		let imagingDiagnosticReports = [];
		let imagingDiagnosticResults = await getImagingStudyReport(args, context, logger);
		if (imagingDiagnosticResults && imagingDiagnosticResults.length !== 0) {
			for (let data of imagingDiagnosticResults) {
				imagingDiagnosticReports.push({fullUrl: `${BASE_URL}/DiagnosticReport/${data.id}`, resource: data});
			}
			diagnosticReports.push(...imagingDiagnosticReports);
		}
		let procedureDiagnosticReports = [];
		let procedureReportResults = await getProcedureReport(args, context, logger);
		if (procedureReportResults && procedureReportResults.length !== 0) {
			for (let data of procedureReportResults) {
				procedureDiagnosticReports.push({fullUrl: `${BASE_URL}/DiagnosticReport/${data.id}`, resource: data});
			}
			diagnosticReports.push(...procedureDiagnosticReports);
		}

		resolve(diagnosticReports);

	} catch (err){
		reject(err);
	}
});
