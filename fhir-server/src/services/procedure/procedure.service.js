/*eslint no-unused-vars: "warn"*/

const { RESOURCES } = require('@asymmetrik/node-fhir-server-core').constants;
const FHIRServer = require('@asymmetrik/node-fhir-server-core');
const globals = require('../../globals');
const { CLIENT } = require('../../constants');

let getProcedure = (base_version) => {
	return require(FHIRServer.resolveFromVersion(base_version, RESOURCES.PROCEDURE));};

let getMeta = (base_version) => {
	return require(FHIRServer.resolveFromVersion(base_version, RESOURCES.META));};

let buildStu3SearchQuery = (args) =>	 {

	// Common search params
	let { base_version, _content, _format, _id, _lastUpdated, _profile, _query, _security, _tag } = args;

	// Search Result params
	let { _INCLUDE, _REVINCLUDE, _SORT, _COUNT, _SUMMARY, _ELEMENTS, _CONTAINED, _CONTAINEDTYPED } = args;

	// Resource Specific params
	let based_on = args['based-on'];
	let category = args['category'];
	let code = args['code'];
	let _context = args['_context'];
	let date = args['date'];
	let definition = args['definition'];
	let encounter = args['encounter'];
	let identifier = args['identifier'];
	let location = args['location'];
	let part_of = args['part-of'];
	let patient = args['patient'];
	let performer = args['performer'];
	let status = args['status'];
	let subject = args['subject'];

	let patientId;
	if (patient){
		let data = patient.split('/');
		patientId = data[1];
	}

	let query = `SELECT   procedure_order.procedure_order_id, procedure_order.order_status,
procedure_order.patient_id,procedure_order.provider_id,procedure_order.date_ordered,procedure_order.encounter_id,
procedure_order_code.procedure_code,procedure_order_code.procedure_name, category_code, category_name,
procedure_order_code.diagnoses AS reasonCode, procedure_order_code.reason, 
procedure_order_code.body_site_code AS bodySiteCode, procedure_order_code.body_site_name AS bodySite,
performer.id AS performerId, performer.fname AS performerFirstName,performer.lname AS performerLastName
FROM procedure_order 
JOIN procedure_order_code ON procedure_order.procedure_order_id = procedure_order_code.procedure_order_id 
LEFT JOIN users AS performer ON procedure_order.provider_id = performer.id `;

	let whereFilter = ' WHERE procedure_order_title = ?';
	let params = ['Procedure'];
	if (patientId){
		if (params.length ) {
			whereFilter += ' AND';
		}
		whereFilter += ' procedure_order.patient_id = ?';
		params.push(patientId);
	}

	let lastUpdated = args['_lastUpdated'];
	if (lastUpdated){
		if (params.length ) {
			whereFilter += ' AND';
		}
		let order = lastUpdated.substring(0, 2);
		if (order === 'lt'){
			whereFilter += ' procedure_order.date_ordered < ? ';
		}
		else {
			whereFilter += ' procedure_order.date_ordered > ? ';
		}

		let lastUpdatedDate = lastUpdated.substr(2);
		params.push(lastUpdatedDate);
	}

	if (params.length) {
		query += whereFilter;
	}

	let sort = args['_sort'];
	if (sort && sort === '_lastUpdated'){
		query += ' ORDER BY procedure_order.date_ordered';
	}

	return {query, params};

};

function mapProcedureResultToProcedureFHIRResource(procedure) {
	let fhirResource = {};
	fhirResource.id = procedure.procedure_order_id;
	fhirResource.status = procedure.order_status;

	fhirResource.category = {
		'coding': [
			{
				'system': 'http://snomed.info/sct',
				'code': procedure.category_code,
				'display': procedure.category_name
			}
		]
	};

	fhirResource.code = {
		'coding': [
			{
				'system': 'http://snomed.info/sct',
				'code': procedure.procedure_code,
				'display': procedure.procedure_name
			}
		]
	};

	fhirResource.context = {
		'id': procedure.encounter_id,
		'reference': `Encounter/${procedure.encounter_id}`
	};

	fhirResource.reasonCode = [{
		'coding': [
			{
				'system': 'http://snomed.info/sct',
				'code': procedure.reasonCode,
				display: procedure.reason
			}
		],
	}];

	fhirResource.bodySite = [
		{
			'coding': [{
				'system': 'http://snomed.info/sct',
				'code': procedure.bodySiteCode,
				'display': procedure.bodySite
			}]
		}];

	fhirResource.performer = [
		{
			actor: {
				'reference': `Practitioner/${procedure.performerId}`,
				'display': `${procedure.performerFirstName} ${procedure.performerLastName}`
			}
		}];

			return fhirResource;
}

module.exports.search = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Procedure >>> search');

	let base_version = '3_0_1';
	let {query, params} = buildStu3SearchQuery(args);

	let connection = globals.get(CLIENT);

	let Procedure = getProcedure(base_version);
	connection.query(query, params, function (err, results, fields){
		if (err) {
			logger.error('Error with Observation.search: ', err);
			reject(err);
		}
		else {
			let response = [];
			if (results && results.length !== 0) {
				for (let data of results) {
					let procedureResult = mapProcedureResultToProcedureFHIRResource(data);
					response.push(new Procedure(procedureResult));
				}
			}
			resolve(response);
		}
		resolve();
	});
});

module.exports.searchById = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Procedure >>> searchById');

	let { base_version, id } = args;

	let Procedure = getProcedure(base_version);

	// TODO: Build query from Parameters

	// TODO: Query database

	// Cast result to Procedure Class
	let procedure_resource = new Procedure();
	// TODO: Set data with constructor or setter methods
	procedure_resource.id = 'test id';

	// Return resource class
	// resolve(procedure_resource);
	resolve();
});

module.exports.create = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Procedure >>> create');

	let { base_version, id, resource } = args;

	let Procedure = getProcedure(base_version);
	let Meta = getMeta(base_version);

	// TODO: determine if client/server sets ID

	// Cast resource to Procedure Class
	let procedure_resource = new Procedure(resource);
	procedure_resource.meta = new Meta();
	// TODO: set meta info

	// TODO: save record to database

	// Return Id
	resolve({ id: procedure_resource.id });
});

module.exports.update = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Procedure >>> update');

	let { base_version, id, resource } = args;

	let Procedure = getProcedure(base_version);
	let Meta = getMeta(base_version);

	// Cast resource to Procedure Class
	let procedure_resource = new Procedure(resource);
	procedure_resource.meta = new Meta();
	// TODO: set meta info, increment meta ID

	// TODO: save record to database

	// Return id, if recorded was created or updated, new meta version id
	resolve({ id: procedure_resource.id, created: false, resource_version: procedure_resource.meta.versionId });
});

module.exports.remove = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Procedure >>> remove');

	let { id } = args;

	// TODO: delete record in database (soft/hard)

	// Return number of records deleted
	resolve({ deleted: 0 });
});

module.exports.searchByVersionId = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Procedure >>> searchByVersionId');

	let { base_version, id, version_id } = args;

	let Procedure = getProcedure(base_version);

	// TODO: Build query from Parameters

	// TODO: Query database

	// Cast result to Procedure Class
	let procedure_resource = new Procedure();

	// Return resource class
	resolve(procedure_resource);
});

module.exports.history = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Procedure >>> history');

	// Common search params
	let { base_version, _content, _format, _id, _lastUpdated, _profile, _query, _security, _tag } = args;

	// Search Result params
	let { _INCLUDE, _REVINCLUDE, _SORT, _COUNT, _SUMMARY, _ELEMENTS, _CONTAINED, _CONTAINEDTYPED } = args;

	// Resource Specific params
	let based_on = args['based-on'];
	let category = args['category'];
	let code = args['code'];
	let _context = args['_context'];
	let date = args['date'];
	let definition = args['definition'];
	let encounter = args['encounter'];
	let identifier = args['identifier'];
	let location = args['location'];
	let part_of = args['part-of'];
	let patient = args['patient'];
	let performer = args['performer'];
	let status = args['status'];
	let subject = args['subject'];

	// TODO: Build query from Parameters

	// TODO: Query database

	let Procedure = getProcedure(base_version);

	// Cast all results to Procedure Class
	let procedure_resource = new Procedure();

	// Return Array
	resolve([procedure_resource]);
});

module.exports.historyById = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Procedure >>> historyById');

	// Common search params
	let { base_version, _content, _format, _id, _lastUpdated, _profile, _query, _security, _tag } = args;

	// Search Result params
	let { _INCLUDE, _REVINCLUDE, _SORT, _COUNT, _SUMMARY, _ELEMENTS, _CONTAINED, _CONTAINEDTYPED } = args;

	// Resource Specific params
	let based_on = args['based-on'];
	let category = args['category'];
	let code = args['code'];
	let _context = args['_context'];
	let date = args['date'];
	let definition = args['definition'];
	let encounter = args['encounter'];
	let identifier = args['identifier'];
	let location = args['location'];
	let part_of = args['part-of'];
	let patient = args['patient'];
	let performer = args['performer'];
	let status = args['status'];
	let subject = args['subject'];

	// TODO: Build query from Parameters

	// TODO: Query database

	let Procedure = getProcedure(base_version);

	// Cast all results to Procedure Class
	let procedure_resource = new Procedure();

	// Return Array
	resolve([procedure_resource]);
});

