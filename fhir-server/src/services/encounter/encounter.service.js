/*eslint no-unused-vars: "warn"*/

const { RESOURCES } = require('@asymmetrik/node-fhir-server-core').constants;
const FHIRServer = require('@asymmetrik/node-fhir-server-core');
const globals = require('../../globals');
const { CLIENT, BASE_URL } = require('../../constants');

let getEncounter = (base_version) => {
	return require(FHIRServer.resolveFromVersion(base_version, RESOURCES.ENCOUNTER));};

let getMeta = (base_version) => {
	return require(FHIRServer.resolveFromVersion(base_version, RESOURCES.META));};

function mapEncounterResultToEncounterFHIRResource(encounter){
    let fhirResource = {id: encounter.id};
    fhirResource.identifier = [
        {use: 'usual', system: 'https://www.open-emr.org', value: encounter.id },
    ];
    fhirResource.subject = {
        'id': encounter.pid,
        'reference': `Patient/${encounter.pid}`,
        'display': encounter.patient_name
    };

    fhirResource.participant = [ {
        'individual': {
            'reference': `Practitioner/${encounter.provider_id}`,
            'display': `${encounter.providerFirstName} ${encounter.providerLastName}`
        },
        period: {start: encounter.date}
    }];

    fhirResource.class = {
        'system': 'http://hl7.org/fhir/ValueSet/v3-ActEncounterCode',
        'code': encounter.class_code,
        display: encounter.class
    };

    fhirResource.priority = {
        'coding': [
            {
                'system': 'http://hl7.org/fhir/ValueSet/v3-ActPriority',
                'code': encounter.priority_code,
                display: encounter.priority
            }
        ]
    };

    fhirResource.reason = [{
        'coding': [
            {
                'system': 'http://snomed.info/sct',
                'code': encounter.reason_code,
                display: encounter.reason_name
            }
        ],
        text: encounter.reason
        }
        ];
    fhirResource.period = {start: encounter.date};

    fhirResource.location = [
        {
            'location': {
                'reference': `Location/${encounter.facility_id}`,
                'display': encounter.facility
            },
            'period': {
                'start': encounter.date,
            }
        }
    ];


    fhirResource.meta = {versionId: 1, lastUpdated: encounter.date};
    return fhirResource;
}

let buildStu3SearchQuery = (args) => {
    // Common search params
    let { base_version, _content, _format, _id, _lastUpdated, _profile, _query, _security, _tag } = args;

    // Search Result params
    let { _INCLUDE, _REVINCLUDE, _SORT, _COUNT, _SUMMARY, _ELEMENTS, _CONTAINED, _CONTAINEDTYPED } = args;

    // Resource Specific params
    let appointment = args['appointment'];
    let _class = args['_class'];
    let date = args['date'];
    let diagnosis = args['diagnosis'];
    let episodeofcare = args['episodeofcare'];
    let identifier = args['identifier'];
    let incomingreferral = args['incomingreferral'];
    let length = args['length'];
    let location = args['location'];
    let location_period = args['location-period'];
    let part_of = args['part-of'];
    let participant = args['participant'];
    let participant_type = args['participant-type'];
    let patient = args['patient'];
    let practitioner = args['practitioner'];
    let reason = args['reason'];
    let service_provider = args['service-provider'];
    let special_arrangement = args['special-arrangement'];
    let status = args['status'];
    let subject = args['subject'];
    let type = args['type'];


    let query = `SELECT fe.encounter as id,
                       fe.date,
                       fe.reason,
                       fe.reason_code,
                       fe.reason_name,
                       fe.facility,
                       fe.facility_id,
                       fe.priority_code,
                       fe.priority,
                       fe.class_code,
                       fe.class,
                       fe.pid,
                       fe.onset_date,
                       fe.sensitivity,
                       fe.billing_note,
                       fe.pc_catid,
                       fe.last_level_billed,
                       fe.last_level_closed,
                       fe.last_stmt_date,
                       fe.stmt_count,
                       fe.provider_id,
                       fe.supervisor_id,
                       fe.invoice_refno,
                       fe.referral_source,
                       fe.billing_facility,
                       fe.external_id,
                       fe.pos_code,
                       opc.pc_catname,
                       fa.name AS billing_facility_name,
                       patient.identifier as patient_identifier,
                       patient.genericname1 as patient_name,
                       provider.id AS providerId, provider.fname AS providerFirstName,provider.lname AS providerLastName
                       FROM form_encounter as fe
                       LEFT JOIN openemr_postcalendar_categories as opc
                       ON opc.pc_catid = fe.pc_catid
                       LEFT JOIN facility as fa ON fa.id = fe.billing_facility
                       LEFT JOIN patient_data as patient ON patient.pid = fe.pid
                       LEFT JOIN users AS provider ON fe.provider_id = provider.id `;
    let params = [];

    let whereFilter = ' WHERE';
    if (identifier){
        whereFilter += ' patient.identifier = ? ';
        params.push(identifier);
	}

    if (subject){
        let data = subject.split('/');
        let patientId = data[1];
        if (params.length) {
            whereFilter += ' AND';
        }
        whereFilter += ' patient.pid = ? ';
        params.push(+patientId);
    }

    let lastUpdated = args['_lastUpdated'];
    if (lastUpdated){
        if (params.length ) {
            whereFilter += ' AND';
        }
        let order = lastUpdated.substring(0, 2);
        if (order === 'lt'){
            whereFilter += ' fe.date < ? ';
        }
        else {
            whereFilter += ' fe.date > ? ';
        }

        let lastUpdatedDate = lastUpdated.substr(2);
        params.push(lastUpdatedDate);
    }

    if (params.length) {
        query += whereFilter;
    }

    let sort = args['_sort'];
    if (sort && sort === '_lastUpdated'){
        query += ' ORDER BY fe.date';
    }

    return {query, params};
};

module.exports.search = (args, context, logger) => new Promise(async(resolve, reject) => {
	logger.info('Encounter >>> search');

    let base_version = '3_0_1';
    let {query, params} = buildStu3SearchQuery(args);


	let Encounter = getEncounter(base_version);
    let connection = globals.get(CLIENT);

    connection.query(query, params, function (err, results, fields){
        if (err) {
            logger.error('Error with Encounter.search: ', err);
            reject(err);
        }
        else {
            let response = [];
            if (results && results.length !== 0) {
                for (let data of results) {
                    let encounterResult = mapEncounterResultToEncounterFHIRResource(data);
                    response.push(new Encounter(encounterResult));
                }
            }
            resolve(response);
        }
    });
});

function getEncounterWithIdQuery(encounterId){
    let query = `SELECT fe.encounter as id,
                       fe.date,
                       fe.reason,
                       fe.facility,
                       fe.facility_id,
                       fe.pid,
                       fe.onset_date,
                       fe.sensitivity,
                       fe.billing_note,
                       fe.pc_catid,
                       fe.last_level_billed,
                       fe.last_level_closed,
                       fe.last_stmt_date,
                       fe.stmt_count,
                       fe.provider_id,
                       fe.supervisor_id,
                       fe.invoice_refno,
                       fe.referral_source,
                       fe.billing_facility,
                       fe.external_id,
                       fe.pos_code,
                       opc.pc_catname,
                       fa.name AS billing_facility_name,
                        patient.identifier as patient_identifier,
                       patient.genericname1 as patient_name
                       FROM form_encounter as fe
                       LEFT JOIN openemr_postcalendar_categories as opc
                       ON opc.pc_catid = fe.pc_catid
                       LEFT JOIN facility as fa ON fa.id = fe.billing_facility
                       LEFT JOIN patient_data as patient ON patient.pid = fe.pid
                       WHERE fe.encounter= ?
                       ORDER BY fe.id
                       DESC;`;

    return {query, params: [encounterId]};
}

module.exports.searchById = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Encounter >>> searchById');

	let { base_version, id } = args;

	let Encounter = getEncounter(base_version);

    let connection = globals.get(CLIENT);
    let {query, params} = getEncounterWithIdQuery(id);
    connection.query(query, params, function (err, results, fields){
        if (err) {
            logger.error('Error with Encounter.searchById: ', err);
            reject(err);
        }
        else {
            if (results[0]) {
                let encounterResult = mapEncounterResultToEncounterFHIRResource(results[0]);
                resolve(new Encounter(encounterResult));
            }
        }
        resolve();
    });
});

module.exports.create = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Encounter >>> create');

	let { base_version, id, resource } = args;

	let Encounter = getEncounter(base_version);
	let Meta = getMeta(base_version);

	// TODO: determine if client/server sets ID

	// Cast resource to Encounter Class
	let encounter_resource = new Encounter(resource);
	encounter_resource.meta = new Meta();
	// TODO: set meta info

	// TODO: save record to database

	// Return Id
	resolve({ id: encounter_resource.id });
});

module.exports.update = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Encounter >>> update');

	let { base_version, id, resource } = args;

	let Encounter = getEncounter(base_version);
	let Meta = getMeta(base_version);

	// Cast resource to Encounter Class
	let encounter_resource = new Encounter(resource);
	encounter_resource.meta = new Meta();
	// TODO: set meta info, increment meta ID

	// TODO: save record to database

	// Return id, if recorded was created or updated, new meta version id
	resolve({ id: encounter_resource.id, created: false, resource_version: encounter_resource.meta.versionId });
});

module.exports.remove = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Encounter >>> remove');

	let { id } = args;

	// TODO: delete record in database (soft/hard)

	// Return number of records deleted
	resolve({ deleted: 0 });
});

module.exports.searchByVersionId = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Encounter >>> searchByVersionId');

	let { base_version, id, version_id } = args;

	let Encounter = getEncounter(base_version);

	// TODO: Build query from Parameters

	// TODO: Query database

	// Cast result to Encounter Class
	let encounter_resource = new Encounter();

	// Return resource class
	resolve(encounter_resource);
});

module.exports.history = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Encounter >>> history');

	// Common search params
	let { base_version, _content, _format, _id, _lastUpdated, _profile, _query, _security, _tag } = args;

	// Search Result params
	let { _INCLUDE, _REVINCLUDE, _SORT, _COUNT, _SUMMARY, _ELEMENTS, _CONTAINED, _CONTAINEDTYPED } = args;

	// Resource Specific params
	let appointment = args['appointment'];
	let _class = args['_class'];
	let date = args['date'];
	let diagnosis = args['diagnosis'];
	let episodeofcare = args['episodeofcare'];
	let identifier = args['identifier'];
	let incomingreferral = args['incomingreferral'];
	let length = args['length'];
	let location = args['location'];
	let location_period = args['location-period'];
	let part_of = args['part-of'];
	let participant = args['participant'];
	let participant_type = args['participant-type'];
	let patient = args['patient'];
	let practitioner = args['practitioner'];
	let reason = args['reason'];
	let service_provider = args['service-provider'];
	let special_arrangement = args['special-arrangement'];
	let status = args['status'];
	let subject = args['subject'];
	let type = args['type'];

	// TODO: Build query from Parameters

	// TODO: Query database

	let Encounter = getEncounter(base_version);

	// Cast all results to Encounter Class
	let encounter_resource = new Encounter();

	// Return Array
	resolve([encounter_resource]);
});

module.exports.historyById = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Encounter >>> historyById');

	// Common search params
	let { base_version, _content, _format, _id, _lastUpdated, _profile, _query, _security, _tag } = args;

	// Search Result params
	let { _INCLUDE, _REVINCLUDE, _SORT, _COUNT, _SUMMARY, _ELEMENTS, _CONTAINED, _CONTAINEDTYPED } = args;

	// Resource Specific params
	let appointment = args['appointment'];
	let _class = args['_class'];
	let date = args['date'];
	let diagnosis = args['diagnosis'];
	let episodeofcare = args['episodeofcare'];
	let identifier = args['identifier'];
	let incomingreferral = args['incomingreferral'];
	let length = args['length'];
	let location = args['location'];
	let location_period = args['location-period'];
	let part_of = args['part-of'];
	let participant = args['participant'];
	let participant_type = args['participant-type'];
	let patient = args['patient'];
	let practitioner = args['practitioner'];
	let reason = args['reason'];
	let service_provider = args['service-provider'];
	let special_arrangement = args['special-arrangement'];
	let status = args['status'];
	let subject = args['subject'];
	let type = args['type'];

	// TODO: Build query from Parameters

	// TODO: Query database

	let Encounter = getEncounter(base_version);

	// Cast all results to Encounter Class
	let encounter_resource = new Encounter();

	// Return Array
	resolve([encounter_resource]);
});


// for get patient everything by id
module.exports.patientEncounters = (args, context, logger) => new Promise(async(resolve, reject) => {
    logger.info('Encounter >>> search');

    let base_version = '3_0_1';
    let {query, params} = buildStu3SearchQuery(args);


    let Encounter = getEncounter(base_version);
    let connection = globals.get(CLIENT);

    connection.query(query, params, function (err, results, fields){
        if (err) {
            logger.error('Error with Encounter.search: ', err);
            reject(err);
        }
        else {
            let response = [];
            if (results && results.length !== 0) {
                for (let data of results) {
                    let encounterResult = mapEncounterResultToEncounterFHIRResource(data);
                    response.push({fullUrl: `${BASE_URL}/Encounter/${encounterResult.id}`, resource: new Encounter(encounterResult)});
                }
            }
            resolve(response);
        }
    });
});
