/*eslint no-unused-vars: "warn"*/

const { RESOURCES } = require('@asymmetrik/node-fhir-server-core').constants;
const FHIRServer = require('@asymmetrik/node-fhir-server-core');
const globals = require('../../globals');
const { CLIENT } = require('../../constants');

let getObservation = (base_version) => {
	return require(FHIRServer.resolveFromVersion(base_version, RESOURCES.OBSERVATION));};

let getMeta = (base_version) => {
	return require(FHIRServer.resolveFromVersion(base_version, RESOURCES.META));};

function mapObservationResultToObservationFHIRResource(observation){
    let fhirResource = {};
    fhirResource.id = observation.id;
    fhirResource.identifier = [
        {use: 'usual', system: 'https://www.open-emr.org', value: observation.id },
    ];
    fhirResource.code = {
        'coding': [],
    };
    if (observation.code_type === 'LOINC'){
        fhirResource.code.coding.push( {
            'system': 'http://loinc.org',
            'code': observation.code,
            display: observation.description
        });
    }
	else if (observation.code_type === 'SNOMED-CT') {
		fhirResource.code.coding.push( {
			'system': 'http://snomed.info/sct',
			'code': observation.code,
			display: observation.description
		});
	}

    fhirResource.subject = {
        'id': observation.pid,
        'reference': `Patient/${observation.pid}`
    };

	fhirResource.category = [{
		'coding': [
			{
				'system': 'http://hl7.org/fhir/observation-category',
				'code': observation.category_code,
				'display': observation.category_name
			}
		]
	}];

    fhirResource.valueQuantity = {
        'value': observation.ob_value,
        'unit': observation.ob_unit,
        'system': 'http://unitsofmeasure.org'
    };

    if (observation.intepretation_code) {
		fhirResource.interpretation = {
			'coding': [
				{
					'system': 'http://hl7.org/fhir/v2/0078',
					'code': observation.intepretation_code,
					'display': observation.intepretation_name
				}
			]
		};
	}


    if (observation.low_range_value || observation.high_range_value){
    	let referenceRange = [];
    	let range = {};
    	if (observation.low_range_value){
			range.low = {
				'value': observation.low_range_value,
				'unit': observation.low_range_unit
			};
		}
		if (observation.high_range_value) {
			range.high = {
				'value': observation.high_range_value,
				'unit': observation.high_range_unit
			};
		}
		referenceRange.push(range);
		fhirResource.referenceRange = referenceRange;
	}

    if (observation.specimen_code){
    	fhirResource.specimen = {
			'display': observation.specimen_name
		};
	}

    if (observation.body_site_code){
		fhirResource.bodySite = {
			'coding': [
				{
					'system': 'http://snomed.info/sct',
					'code': observation.body_site_code,
					display: observation.body_site_name
				}
			]
		};
	}

    fhirResource.issued = observation.date;
    fhirResource.comment = observation.observation;
    fhirResource.meta = {versionId: 1, lastUpdated: observation.date};

    return fhirResource;
}

let buildStu3SearchQuery = (args) => {
// Common search params
    let { base_version, _content, _format, _id, _lastUpdated, _profile, _query, _security, _tag } = args;

    // Search Result params
    let { _INCLUDE, _REVINCLUDE, _SORT, _COUNT, _SUMMARY, _ELEMENTS, _CONTAINED, _CONTAINEDTYPED } = args;

    // Resource Specific params
    let based_on = args['based-on'];
    let category = args['category'];
    let code = args['code'];
    let code_value_concept = args['code-value-concept'];
    let code_value_date = args['code-value-date'];
    let code_value_quantity = args['code-value-quantity'];
    let code_value_string = args['code-value-string'];
    let combo_code = args['combo-code'];
    let combo_code_value_concept = args['combo-code-value-concept'];
    let combo_code_value_quantity = args['combo-code-value-quantity'];
    let combo_data_absent_reason = args['combo-data-absent-reason'];
    let combo_value_concept = args['combo-value-concept'];
    let combo_value_quantity = args['combo-value-quantity'];
    let component_code = args['component-code'];
    let component_code_value_concept = args['component-code-value-concept'];
    let component_code_value_quantity = args['component-code-value-quantity'];
    let component_data_absent_reason = args['component-data-absent-reason'];
    let component_value_concept = args['component-value-concept'];
    let component_value_quantity = args['component-value-quantity'];
    let _context = args['_context'];
    let data_absent_reason = args['data-absent-reason'];
    let date = args['date'];
    let device = args['device'];
    let encounter = args['encounter'];
    let identifier = args['identifier'];
    let method = args['method'];
    let patient = args['patient'];
    let subject = args['subject'];
    let performer = args['performer'];
    let related = args['related'];
    let related_target = args['related-target'];
    let related_type = args['related-type'];
    let specimen = args['specimen'];
    let status = args['status'];
    let reference = args['reference'];
    let value_concept = args['value-concept'];
    let value_date = args['value-date'];
    let value_quantity = args['value-quantity'];
    let value_string = args['value-string'];

    let patientId;
    if (subject){
    	let data = subject.split('/');
    	patientId = data[1];
	} else if (patient){
		let data = patient.split('/');
		patientId = data[1];
	}

    let query = `SELECT fm.id,
        fm.pid,
        fm.date,
        fm.encounter,
        fm.groupname,
        fm.code,
        fm.observation,
        fm.ob_value,
        fm.ob_unit,
        fm.description,
        fm.specimen_code ,
        fm.specimen_name,
        fm.body_site_code,
        fm.body_site_name,
        fm.low_range_value,
        fm.low_range_unit,
        fm.high_range_value,
        fm.high_range_unit,
        fm.category_code,
        fm.category_name,
        fm.intepretation_code,
        fm.intepretation_name,
    	patient.genericname1 as patient_name,
        fm.code_type FROM form_observation AS fm
    LEFT JOIN patient_data as patient ON patient.pid = fm.pid`;

	let whereFilter = ' WHERE';
    let params = [];
    if (patientId){
		whereFilter += ' fm.pid = ? ';
        params.push(patientId);
	}

    if(category){
		if (params.length ) {
			whereFilter += ' AND';
		}
		whereFilter += ' fm.category_code = ? ';
		params.push(category);
	}

	let lastUpdated = args['_lastUpdated'];
	if (lastUpdated){
		if (params.length ) {
			whereFilter += ' AND';
		}
		let order = lastUpdated.substring(0, 2);
		if (order === 'lt'){
			whereFilter += ' fm.date < ? ';
		}
		else {
			whereFilter += ' fm.date > ? ';
		}

		let lastUpdatedDate = lastUpdated.substr(2);
		params.push(lastUpdatedDate);
	}

	if (params.length) {
		query += whereFilter;
	}

	let sort = args['_sort'];
	if (sort && sort === '_lastUpdated'){
		query += ' ORDER BY fm.date';
	}


	return {query, params};
};

module.exports.search = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Observation >>> search');


	let base_version = '3_0_1';
    let {query, params} = buildStu3SearchQuery(args);


	let Observation = getObservation(base_version);
    let connection = globals.get(CLIENT);
    connection.query(query, params, function (err, results, fields){
        if (err) {
            logger.error('Error with Observation.search: ', err);
            reject(err);
        }
        else {
        	let response = [];
            if (results && results.length !== 0) {
            	for (let data of results) {
                    let observationResult = mapObservationResultToObservationFHIRResource(data);
                    response.push(new Observation(observationResult));
                }
            }
            resolve(response);
        }
        resolve();
    });
});

function getObservationWithIdQuery(observationId){
    let query = `SELECT fm.id,
        fm.pid,
        fm.date,
        fm.encounter,
        fm.groupname,
        fm.code,
        fm.observation,
        fm.ob_value,
        fm.ob_unit,
        fm.description,
        fm.specimen_code ,
        fm.specimen_name,
        fm.body_site_code,
        fm.body_site_name,
        fm.low_range_value,
        fm.low_range_unit,
        fm.high_range_value,
        fm.high_range_unit,
        fm.category_code,
        fm.category_name,
        fm.intepretation_code,
        fm.intepretation_name,
    	patient.genericname1 as patient_name,
        fm.code_type FROM form_observation AS fm
    LEFT JOIN patient_data as patient ON patient.pid = fm.pid
        WHERE fm.id = ?;`;

    return {query, params: [observationId]};
}

module.exports.searchById = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Observation >>> searchById');

	let { base_version, id } = args;

	let Observation = getObservation(base_version);

    let connection = globals.get(CLIENT);
    let {query, params} = getObservationWithIdQuery(id);
    connection.query(query, params, function (err, results, fields){
        if (err) {
            logger.error('Error with Observation.searchById: ', err);
            reject(err);
        }
        else {
            if (results[0]) {
                let observationResult = mapObservationResultToObservationFHIRResource(results[0]);
                resolve(new Observation(observationResult));
            }
        }
        resolve();
    });
});

module.exports.create = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Observation >>> create');

	let { base_version, id, resource } = args;

	let Observation = getObservation(base_version);
	let Meta = getMeta(base_version);

	// TODO: determine if client/server sets ID

	// Cast resource to Observation Class
	let observation_resource = new Observation(resource);
	observation_resource.meta = new Meta();
	// TODO: set meta info

	// TODO: save record to database

	// Return Id
	resolve({ id: observation_resource.id });
});

module.exports.update = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Observation >>> update');

	let { base_version, id, resource } = args;

	let Observation = getObservation(base_version);
	let Meta = getMeta(base_version);

	// Cast resource to Observation Class
	let observation_resource = new Observation(resource);
	observation_resource.meta = new Meta();
	// TODO: set meta info, increment meta ID

	// TODO: save record to database

	// Return id, if recorded was created or updated, new meta version id
	resolve({ id: observation_resource.id, created: false, resource_version: observation_resource.meta.versionId });
});

module.exports.remove = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Observation >>> remove');

	let { id } = args;

	// TODO: delete record in database (soft/hard)

	// Return number of records deleted
	resolve({ deleted: 0 });
});

module.exports.searchByVersionId = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Observation >>> searchByVersionId');

	let { base_version, id, version_id } = args;

	let Observation = getObservation(base_version);

	// TODO: Build query from Parameters

	// TODO: Query database

	// Cast result to Observation Class
	let observation_resource = new Observation();

	// Return resource class
	resolve(observation_resource);
});

module.exports.history = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Observation >>> history');

	// Common search params
	let { base_version, _content, _format, _id, _lastUpdated, _profile, _query, _security, _tag } = args;

	// Search Result params
	let { _INCLUDE, _REVINCLUDE, _SORT, _COUNT, _SUMMARY, _ELEMENTS, _CONTAINED, _CONTAINEDTYPED } = args;

	// Resource Specific params
	let based_on = args['based-on'];
	let category = args['category'];
	let code = args['code'];
	let code_value_concept = args['code-value-concept'];
	let code_value_date = args['code-value-date'];
	let code_value_quantity = args['code-value-quantity'];
	let code_value_string = args['code-value-string'];
	let combo_code = args['combo-code'];
	let combo_code_value_concept = args['combo-code-value-concept'];
	let combo_code_value_quantity = args['combo-code-value-quantity'];
	let combo_data_absent_reason = args['combo-data-absent-reason'];
	let combo_value_concept = args['combo-value-concept'];
	let combo_value_quantity = args['combo-value-quantity'];
	let component_code = args['component-code'];
	let component_code_value_concept = args['component-code-value-concept'];
	let component_code_value_quantity = args['component-code-value-quantity'];
	let component_data_absent_reason = args['component-data-absent-reason'];
	let component_value_concept = args['component-value-concept'];
	let component_value_quantity = args['component-value-quantity'];
	let _context = args['_context'];
	let data_absent_reason = args['data-absent-reason'];
	let date = args['date'];
	let device = args['device'];
	let encounter = args['encounter'];
	let identifier = args['identifier'];
	let method = args['method'];
	let patient = args['patient'];
	let performer = args['performer'];
	let related = args['related'];
	let related_target = args['related-target'];
	let related_type = args['related-type'];
	let specimen = args['specimen'];
	let status = args['status'];
	let reference = args['reference'];
	let value_concept = args['value-concept'];
	let value_date = args['value-date'];
	let value_quantity = args['value-quantity'];
	let value_string = args['value-string'];

	// TODO: Build query from Parameters

	// TODO: Query database

	let Observation = getObservation(base_version);

	// Cast all results to Observation Class
	let observation_resource = new Observation();

	// Return Array
	resolve([observation_resource]);
});

module.exports.historyById = (args, context, logger) => new Promise((resolve, reject) => {
	logger.info('Observation >>> historyById');

	// Common search params
	let { base_version, _content, _format, _id, _lastUpdated, _profile, _query, _security, _tag } = args;

	// Search Result params
	let { _INCLUDE, _REVINCLUDE, _SORT, _COUNT, _SUMMARY, _ELEMENTS, _CONTAINED, _CONTAINEDTYPED } = args;

	// Resource Specific params
	let based_on = args['based-on'];
	let category = args['category'];
	let code = args['code'];
	let code_value_concept = args['code-value-concept'];
	let code_value_date = args['code-value-date'];
	let code_value_quantity = args['code-value-quantity'];
	let code_value_string = args['code-value-string'];
	let combo_code = args['combo-code'];
	let combo_code_value_concept = args['combo-code-value-concept'];
	let combo_code_value_quantity = args['combo-code-value-quantity'];
	let combo_data_absent_reason = args['combo-data-absent-reason'];
	let combo_value_concept = args['combo-value-concept'];
	let combo_value_quantity = args['combo-value-quantity'];
	let component_code = args['component-code'];
	let component_code_value_concept = args['component-code-value-concept'];
	let component_code_value_quantity = args['component-code-value-quantity'];
	let component_data_absent_reason = args['component-data-absent-reason'];
	let component_value_concept = args['component-value-concept'];
	let component_value_quantity = args['component-value-quantity'];
	let _context = args['_context'];
	let data_absent_reason = args['data-absent-reason'];
	let date = args['date'];
	let device = args['device'];
	let encounter = args['encounter'];
	let identifier = args['identifier'];
	let method = args['method'];
	let patient = args['patient'];
	let performer = args['performer'];
	let related = args['related'];
	let related_target = args['related-target'];
	let related_type = args['related-type'];
	let specimen = args['specimen'];
	let status = args['status'];
	let reference = args['reference'];
	let value_concept = args['value-concept'];
	let value_date = args['value-date'];
	let value_quantity = args['value-quantity'];
	let value_string = args['value-string'];

	// TODO: Build query from Parameters

	// TODO: Query database

	let Observation = getObservation(base_version);

	// Cast all results to Observation Class
	let observation_resource = new Observation();

	// Return Array
	resolve([observation_resource]);
});

