ALTER TABLE `openemr`.`prescriptions`
ADD COLUMN `drug_code` VARCHAR(45) NULL AFTER `txDate`,
ADD COLUMN `reason_code` VARCHAR(45) NULL AFTER `drug_code`,
ADD COLUMN `reason` VARCHAR(45) NULL AFTER `reason_code`,
ADD COLUMN `priority` VARCHAR(45) NULL AFTER `reason`,
ADD COLUMN `dosage_timing_code` VARCHAR(45) NULL AFTER `priority`,
ADD COLUMN `dosage_timing_frequency` VARCHAR(45) NULL AFTER `dosage_timing_code`,
ADD COLUMN `dosage_timing_period` VARCHAR(45) NULL AFTER `dosage_timing_frequency`,
ADD COLUMN `dosage_timing_period_unit` VARCHAR(45) NULL AFTER `dosage_timing_period`,
ADD COLUMN `additional_instruction_code` VARCHAR(45) NULL AFTER `dosage_timing_period_unit`,
ADD COLUMN `additional_instruction` VARCHAR(45) NULL AFTER `additional_instruction_code`,
ADD COLUMN `drug_quantity_unit` VARCHAR(45) NULL AFTER `additional_instruction`,
ADD COLUMN `expected_supply_duration` INT NULL AFTER `drug_quantity_unit`,
ADD COLUMN `expected_supply_duration_unit` VARCHAR(45) NULL AFTER `expected_supply_duration`,
ADD COLUMN `dispense_quantity` VARCHAR(45) NULL AFTER `expected_supply_duration_unit`,
ADD COLUMN `dispense_quantity_unit` VARCHAR(45) NULL AFTER `dispense_quantity`;

ALTER TABLE `openemr`.`prescriptions`
ADD COLUMN `drug_route_name` VARCHAR(45) NULL AFTER `dispense_quantity_unit`,
ADD COLUMN `drug_route_code` VARCHAR(45) NULL AFTER `drug_route_name`;
