DROP TABLE IF EXISTS `imaging_order`;
CREATE TABLE `imaging_order` (
  `imaging_order_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `provider_id` bigint(20) NOT NULL DEFAULT '0' COMMENT 'references users.id, the ordering provider',
  `patient_id` bigint(20) NOT NULL COMMENT 'references patient_data.pid',
  `encounter_id` bigint(20) NOT NULL DEFAULT '0' COMMENT 'references form_encounter.encounter',
  `date_ordered` date DEFAULT NULL,
  `order_priority` varchar(31) NOT NULL DEFAULT '',
  `order_status` varchar(31) NOT NULL DEFAULT '' COMMENT 'pending,routed,complete,canceled',
  `patient_instructions` text,
  `activity` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0 if deleted',
  `accession_number` varchar(255) NOT NULL ,
  `lab_id` bigint(20) NOT NULL DEFAULT '0' COMMENT 'references procedure_providers.ppid',
  `date_transmitted` datetime DEFAULT NULL COMMENT 'time of order transmission, null if unsent',
  `clinical_hx` varchar(255) NOT NULL DEFAULT '' COMMENT 'clinical history text that may be relevant to the order',
  `external_id` varchar(20) DEFAULT NULL,
  `history_order` enum('0','1') DEFAULT '0' COMMENT 'references order is added for history purpose only.',
  PRIMARY KEY (`imaging_order_id`),
  KEY `datepid` (`date_ordered`,`patient_id`),
  KEY `patient_id` (`patient_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `imaging_order_code`;
CREATE TABLE `imaging_order_code` (
  `imaging_order_id` bigint(20) NOT NULL COMMENT 'references imaging_order.imaging_order_id',
  `imaging_order_seq` int(11) NOT NULL COMMENT 'Supports multiple tests per order. imaging_order_seq, incremented in code',
  `procedure_code` varchar(31) NOT NULL DEFAULT '' COMMENT 'like procedure_type.procedure_code',
  `procedure_name` varchar(255) NOT NULL DEFAULT '' COMMENT 'descriptive name of the procedure code',
  `procedure_source` char(1) NOT NULL DEFAULT '1' COMMENT '1=original order, 2=added after order sent',
  `diagnoses` text COMMENT 'diagnoses and maybe other coding (e.g. ICD9:111.11)',
  `do_not_send` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = normal, 1 = do not transmit to lab',
  `procedure_order_title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`imaging_order_id`,`imaging_order_seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `dicom_modality`;
CREATE TABLE `openemr`.`dicom_modality` (
  `id` INT NOT NULL,
  `code` VARCHAR(10) NULL,
  `name` VARCHAR(200) NULL,
  PRIMARY KEY (`id`));

INSERT INTO `openemr`.`dicom_modality` (`id`, `code`, `name`) VALUES ('1', 'CT', 'Computed Tomography');
INSERT INTO `openemr`.`dicom_modality` (`id`, `code`, `name`) VALUES ('2', 'MR', 'Magnetic Resonance');
INSERT INTO `openemr`.`dicom_modality` (`id`, `code`, `name`) VALUES ('3', 'US', 'Ultrasound');
INSERT INTO `openemr`.`dicom_modality` (`id`, `code`, `name`) VALUES ('4', 'OT', 'Other');
INSERT INTO `openemr`.`dicom_modality` (`id`, `code`, `name`) VALUES ('5', 'ES', 'Endoscopy');
INSERT INTO `openemr`.`dicom_modality` (`id`, `code`, `name`) VALUES ('6', 'PT', 'Positron emission tomography');
INSERT INTO `openemr`.`dicom_modality` (`id`, `code`, `name`) VALUES ('7', 'XA', 'X-Ray Angiography');
INSERT INTO `openemr`.`dicom_modality` (`id`, `code`, `name`) VALUES ('8', 'NM', 'Nuclear Medicine');
INSERT INTO `openemr`.`dicom_modality` (`id`, `code`, `name`) VALUES ('9', 'MG', 'Mammography');


INSERT INTO `openemr`.`list_options` (`list_id`, `option_id`, `title`, `seq`, `is_default`, `option_value`, `mapping`, `codes`, `toggle_setting_1`, `toggle_setting_2`, `activity`, `subtype`, `edit_options`, `timestamp`) VALUES ('proc_type', 'img_ord', 'Imaging Order', '70', '0', '0', '', '', '0', '0', '1', '', '1', '2018-12-10 18:30:24');


ALTER TABLE `openemr`.`imaging_order_code`
ADD COLUMN `modality` VARCHAR(10) NOT NULL AFTER `procedure_order_title`;

ALTER TABLE `openemr`.`procedure_type`
ADD COLUMN `modality` VARCHAR(10) NULL AFTER `notes`;


ALTER TABLE `openemr`.`imaging_order_code`
ADD COLUMN `reason` VARCHAR(150) NULL AFTER `modality`;



