ALTER TABLE `openemr`.`procedure_order_code`
ADD COLUMN `specimen_code` VARCHAR(45) NULL AFTER `procedure_order_title`,
ADD COLUMN `specimen_name` VARCHAR(100) NULL AFTER `specimen_code`;

ALTER TABLE `openemr`.`procedure_order_code`
ADD COLUMN `reason` VARCHAR(150) NULL ;
