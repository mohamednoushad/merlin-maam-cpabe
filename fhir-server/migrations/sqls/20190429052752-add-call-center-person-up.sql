INSERT INTO `openemr`.`gacl_aro_groups` (`id`, `parent_id`, `lft`, `rgt`, `name`, `value`) VALUES ('17', '10', '12', '13', 'Call Center', 'answr');

INSERT INTO `openemr`.`users` (`id`, `username`, `password`, `authorized`, `fname`, `lname`, `facility`, `facility_id`, `see_auth`, `active`, `email_direct`, `cal_ui`, `taxonomy`, `calendar`, `abook_type`, `default_warehouse`, `irnpool`, `main_menu_role`, `patient_menu_role`) VALUES ('8', 'callcenter', 'NoLongerUsed', '1', 'Michael', 'Clarke', 'Abc Clinic', '3', '1', '1', '', '3', '207Q00000X', '1', '', '', '', 'answering_service', 'standard');

INSERT INTO `openemr`.`users_secure` (`id`, `username`, `password`, `salt`, `last_update`) VALUES ('8', 'callcenter', '$2a$05$DGmvibdJletCJf5Fe8CzY.QSV.tNdUWIkrvxttZFFuvUGyi8vtbwS', '$2a$05$DGmvibdJletCJf5Fe8CzY/$', '2018-12-10 18:30:45');

INSERT INTO `openemr`.`gacl_aro` (`id`, `section_value`, `value`, `order_value`, `name`, `hidden`) VALUES ('14', 'users', 'callcenter', '10', 'Answering Service', '0');

INSERT INTO `openemr`.`gacl_groups_aro_map` (`group_id`, `aro_id`) VALUES ('17', '14');

INSERT INTO `openemr`.`groups` (`id`, `name`, `user`) VALUES ('8', 'Default', 'callcenter');


INSERT INTO `openemr`.`gacl_acl` (`id`, `section_value`, `allow`, `enabled`, `return_value`, `note`, `updated_date`) VALUES ('32', 'system', '1', '1', 'view', 'Things that call center person can only read', '1544446848');
INSERT INTO `openemr`.`gacl_acl` (`id`, `section_value`, `allow`, `enabled`, `return_value`, `note`, `updated_date`) VALUES ('33', 'system', '1', '1', 'addonly', 'Things that call center person can read and enter but not modify', '1544446848');
INSERT INTO `openemr`.`gacl_acl` (`id`, `section_value`, `allow`, `enabled`, `return_value`, `note`, `updated_date`) VALUES ('34', 'system', '1', '1', 'wsome', 'Things that call center person can read and partly modify', '1544446848');
INSERT INTO `openemr`.`gacl_acl` (`id`, `section_value`, `allow`, `enabled`, `return_value`, `note`, `updated_date`) VALUES ('35', 'system', '1', '1', 'write', 'Things that call center person can read and modify', '1544446848');

INSERT INTO `openemr`.`gacl_aro_groups_map` (`acl_id`, `group_id`) VALUES ('32', '17');
INSERT INTO `openemr`.`gacl_aro_groups_map` (`acl_id`, `group_id`) VALUES ('33', '17');
INSERT INTO `openemr`.`gacl_aro_groups_map` (`acl_id`, `group_id`) VALUES ('34', '17');
INSERT INTO `openemr`.`gacl_aro_groups_map` (`acl_id`, `group_id`) VALUES ('35', '17');

INSERT INTO `openemr`.`gacl_aco_map` (`acl_id`, `section_value`, `value`) VALUES ('35', 'patients', 'appt');
INSERT INTO `openemr`.`gacl_aco_map` (`acl_id`, `section_value`, `value`) VALUES ('35', 'patients', 'notes');
INSERT INTO `openemr`.`gacl_aco_map` (`acl_id`, `section_value`, `value`) VALUES ('35', 'patients', 'demo');

