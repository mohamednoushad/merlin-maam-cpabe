INSERT INTO `openemr`.`users` (`id`, `username`, `password`, `authorized`, `fname`, `lname`, `facility`, `facility_id`, `see_auth`, `active`, `email_direct`, `cal_ui`, `taxonomy`, `calendar`, `abook_type`, `default_warehouse`, `irnpool`, `main_menu_role`, `patient_menu_role`) VALUES ('5', 'physician', 'NoLongerUsed', '1', 'Roy', 'Thomas', 'Abc Clinic', '3', '1', '1', '', '3', '207Q00000X', '1', '', '', '', 'standard', 'standard');

INSERT INTO `openemr`.`users_secure` (`id`, `username`, `password`, `salt`, `last_update`) VALUES ('5', 'physician', '$2a$05$DGmvibdJletCJf5Fe8CzY.QSV.tNdUWIkrvxttZFFuvUGyi8vtbwS', '$2a$05$DGmvibdJletCJf5Fe8CzY/$', '2018-12-10 18:30:45');

INSERT INTO `openemr`.`gacl_aro` (`id`, `section_value`, `value`, `order_value`, `name`, `hidden`) VALUES ('11', 'users', 'physician', '13', 'Physician', '0');

INSERT INTO `openemr`.`gacl_groups_aro_map` (`group_id`, `aro_id`) VALUES ('13', '11');

INSERT INTO `openemr`.`groups` (`id`, `name`, `user`) VALUES ('5', 'Default', 'physician');

-- add privilege

INSERT INTO `openemr`.`gacl_aco` (`id`, `section_value`, `value`, `order_value`, `name`, `hidden`) VALUES ('67', 'physician', 'imaging', '10', 'Radiology', '0');

INSERT INTO `openemr`.`gacl_aco_map` (`acl_id`, `section_value`, `value`) VALUES ('14', 'physician', 'imaging');
