let platformServerUrl = "http://localhost:5000/api";

function login() {

    let bodyData = {
        "userType":$('#usertype').val(),
        "username":$('#username').val(),
        "password":$('#pass').val()
    };
    
    
    $.ajax({
        url: platformServerUrl+'/user/login',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(bodyData),
        success: function (data) {
            if(data.message == "success"){
                     if(data.userType == "doctor") {
                    sessionStorage.setItem("doctorDetails", JSON.stringify(data.userDetails));
                    window.location.href = "/doctorLogin";
                } else {
                    sessionStorage.setItem("patientDetails", JSON.stringify(data.userDetails));
                    window.location.href = "/patientLogin";
                }
                
            }
        }
    });
}