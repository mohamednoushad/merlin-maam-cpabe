const platformServerUrl = "http://localhost:5000/api";
const cpabeServerUrl = "http://localhost:4000";

function signup() {

    let bodyData = {
        "userType":$('#usertype').val(),
        "username":$('#username').val(),
        "password":$('#pass').val()
    };
   
    if($('#usertype').val() == 'patient') {

        bodyData.firstName = $('#fname').val();
        bodyData.familyName = $('#lname').val();
        bodyData.gender = $('#gender').val();
        bodyData.dob = $('#dob').val();
        bodyData.addressLine1 = $('#adrLine1').val();
        bodyData.addressLine2 = $('#adrLine2').val();
        bodyData.city = $('#city').val();
        bodyData.state = $('#state').val();
        bodyData.areaCode = $('#usertype').val();
        bodyData.country = $('#usertype').val();
    }
    
    $.ajax({
        url: platformServerUrl+'/user/register',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(bodyData),
        success: function (data) {
            alert(data.message);
            if(data.message == "success") {
                alert("Your Unique Id is :"+data.userId)
                window.location.href = `${cpabeServerUrl}/generateprivatekey?name=${$('#username').val()}&user=${$('#usertype').val()}&id=${data.userId}`;
                alert ("Please safekeep the private key file you have downloaded");
            }
        }
    });
}


