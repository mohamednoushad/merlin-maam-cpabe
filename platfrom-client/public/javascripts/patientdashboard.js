const platformServerUrl = 'http://localhost:5000/api';
let cpabeServerUrl = 'http://localhost:4000';

let medicationData;
let patientUsername;
let patientAddress;


function logOut() {

    sessionStorage.removeItem("patientDetails");
    window.location.href = "/";

}


function encryptFile() {

    let doctorAddress = $("#shareAddress").val();

    let bodyData = {
        "address":doctorAddress
    };
    
    
    $.ajax({
        url: platformServerUrl+'/user/patient/getDocotorIdFromAddress',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify(bodyData),
        success: function (data) {
            window.location.href = `${cpabeServerUrl}/encryptjsonfile?doctorId=${data.id}&data='${JSON.stringify(medicationData)}'`;
        
        }
    });


}

function giveAccessToDoctor(downloadUrl) {

    let doctorAddress = $("#shareAddress").val();

    let accessRevocationMultiple = $("#accessRevocationTime").val();

    let timeOptions = $("#times").val();

    let startTime = Date.now(); 
    startTimeReducedString = startTime.toString().substring(0,10); //only to check

    console.log("start Time",startTime);
    console.log("reduced",startTime.toString().substring(0,10))

    let endTime;

    
    console.log(doctorAddress);

    console.log($("#accessRevocationTime").val());

    console.log($("#times").val());

    switch(timeOptions) {
        case "minute":
            endTime = +(startTimeReducedString) + accessRevocationMultiple * 60;
            break;
        case "hour":
            endTime = +(startTime) + accessRevocationMultiple * 60 * 60;
            break;
        default:
            break;
      }

      console.log(timeOptions,endTime);


    $.get(`${platformServerUrl}/user/patient/giveAccess?doctorAddress=${doctorAddress}&downloadUrl=${encodeURI(downloadUrl)}&patient=${patientUsername}&expirytime=${endTime}`, function(data){

        if(data.message =="success") {
            alert("Succesfully Shared The Document");
        }

    });
}

function revokeAccess() {

    $.get(`${platformServerUrl}/user/patient/revokeAccess?patientAddr=${patientAddress}`, function(data){

        if(data.message =="success") {
            alert("Succesfully Revoked The Access");
        }

    });




}


$(document).ready(function() {

    var patientDetails = sessionStorage.getItem("patientDetails");
    let details = JSON.parse(patientDetails);
    patientUsername = details.username;
    $("#patientAddress").html(details.address);
    patientAddress = details.address;

    $.get(`${platformServerUrl}/user/patient/profile?identifier=${details.identifier}`, function(data){

        let firstName = data.patientProfile.entry[0].resource.name[0].given[0];
        let familyName = data.patientProfile.entry[0].resource.name[0].family;
 
        $("#welcomeMsg").html("Welcome "+firstName+" "+familyName);

        let id = data.patientProfile.entry[0].resource.id;

        $.get(`${platformServerUrl}/user//patient/medication?id=${id}`, function(medicationResult){

            medicationData = medicationResult.medications;
            let drug = medicationData.entry[0].resource.medicationCodeableConcept.text;
            $("#drugName").html(drug);
            let dosage = medicationData.entry[0].resource.dosageInstruction[0].doseQuantity.value;
            let intakeFrequency = medicationData.entry[0].resource.dosageInstruction[0].timing.code.coding[0].display;
            $("#dosage").html(dosage+ " in "+intakeFrequency);
            let providerName = medicationData.entry[0].resource.recorder.display;
            $("#prescribedDoctor").html(providerName);

        });

    });

});