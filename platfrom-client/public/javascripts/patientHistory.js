const platformServerUrl = 'http://localhost:5000/api';







$(document).ready(function() {

    var patientDetails = sessionStorage.getItem("patientDetails");
    let details = JSON.parse(patientDetails);

    console.log(details);

    $.get(`${platformServerUrl}/events/accessGiven?patientAddress=${details.address}`, function(data){

        let events = data.events;

        events.forEach(element => {

            console.log(element);

            var action;

            let actType = element.eventType;

            if(actType == "accessGiven") {

                action = "Access Given";

            } else {

                action = "Access Revoked";

            }

            $('#dataTable tr:last').after(`<tr><td>${action}</td><td>${element.doctor}</td><td>${element.date}</td></tr>`);
            
        });
    });


    $.get(`${platformServerUrl}/events/accessRevoked?patientAddress=${details.address}`, function(data){

        let events = data.events;

        events.forEach(element => {

            console.log(element);

            var action;

            let actType = element.eventType;

            if(actType == "accessGiven") {

                action = "Access Given";

            } else {

                action = "Access Revoked";

            }

            $('#dataTable tr:last').after(`<tr><td>${action}</td><td>${element.doctor}</td><td>${element.date}</td></tr>`);
            
        });

       

        

    });
  
 

});