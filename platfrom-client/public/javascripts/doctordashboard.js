const platformServerUrl = 'http://localhost:5000/api';

let accessStatus;

let doctorUniqueAddress;

function getPatientFile() {

    console.log("calling get patient file");

    if(accessStatus){

        $.get(`${platformServerUrl}/user/doctor/getFileUrl?doctorAddress=${doctorUniqueAddress}`, function(data){

            console.log(data);

            if(data.message == "success") {

                if (data.url == "") {

                    alert("Time Expired");

                } else {

                    console.log(data);
                    window.location.href = data.url;

                }

                



            } else {
                alert("Something Went Wrong !")
            }

        });

        //write the logic to get the file

    }else{
        alert("No Patient Has Given Access");
    }
    
}



function logOut() {

    sessionStorage.removeItem("doctorDetails");
    window.location.href = "/";
}



$(document).ready(function() {
    var doctorDetails = sessionStorage.getItem("doctorDetails");
    let details = JSON.parse(doctorDetails);
    doctorUniqueAddress = details.address;
    $("#myAddress").html(details.address);


    $.get(`${platformServerUrl}/user/doctor/hasAnyOneGivenAccess?address=${details.address}`, function(data){
        if(data.message=="noaccessgiven"){
            accessStatus= false;
            $("#accessStatus").html("No Patient Has Given Access");
            $("#patientName").html("");
            $("#patientAddress").html("");
        } else {
            accessStatus = true;
            $("#accessStatus").html("Patient Has Given Access");
            $("#patientName").html(data.patientName);
            $("#patientAddress").html(data.patientAddress);

        }
      });


    



});