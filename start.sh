cd $PWD/platfrom-client
pm2 start npm --name client -- start

xdg-open http://localhost:2000/

cd ..

cd $PWD/platform-server
pm2 start index.js

cd ..

cd $PWD/fhir-server
pm2 start npm --name fhir -- start

if [ $(docker ps -a -q) ]
then
    docker stop $(docker ps -a -q)
    docker rm $(docker ps -a -q)
fi

docker run -p 4000:4000 -d noushadmohamed/cp-abe:final2 

sleep 5

curl http://localhost:4000/generate